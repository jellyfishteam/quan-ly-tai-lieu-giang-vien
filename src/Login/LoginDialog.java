package Login;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import javafx.util.Pair;
import managedocumentoflecturer.Method;

/**
 *
 * @author nmhillusion
 */
public class LoginDialog{
    private Dialog<Pair<String, String>> dialog;
    private boolean hasAccount = false, error = true;
    private String usr, pss, pss2;
    private boolean successLogin;
    private Connection con;
    
    public LoginDialog() {
        con = Method.getConnection();
        Statement s;
        try {
            s = con.createStatement();
            ResultSet rs = s.executeQuery("select * from account");
            
            if(rs.next()){
                usr = rs.getString("username");
                pss = rs.getString("password");
                pss2 = rs.getString("password2");
                hasAccount = true;
            }else{
                registerAccount();
                hasAccount = false;
                return;
            }
            
        } catch (SQLException ex) {
            System.out.println("// error in get data from database!");
        }
    }
    
    public String getUserName(){
        return usr;
    }
    
    public boolean display(){
        if(!hasAccount) return false;
        dialog = new Dialog<>();
        dialog.setTitle("Login Dialog");
        dialog.setHeaderText("Please login...");
        ((Stage)(dialog.getDialogPane().getScene().getWindow())).getIcons().add(Method.ImageFromFile("resource/icon.png"));

        // Set the icon (must be included in the project).
        //dialog.setGraphic(new ImageView(this.getClass().getResource("login.png").toString()));

        // Set the button types.
        ButtonType loginButtonType = new ButtonType("Login", ButtonBar.ButtonData.OK_DONE);
        dialog.getDialogPane().getButtonTypes().addAll(loginButtonType, ButtonType.CANCEL);
        
        //  set style
        dialog.getDialogPane().getStylesheets().add("Login/styleLoginDialog.css");
        
        // Create the username and password labels and fields.
        GridPane grid = new GridPane();
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(20, 150, 10, 10));

        TextField username = new TextField(usr);
        username.setPromptText("Username");
        PasswordField password = new PasswordField();
        password.setPromptText("Password");
        
        Label lblfgtPass = new Label("forget password");
        lblfgtPass.setId("lbl-forget-pass");
        lblfgtPass.setOnMousePressed((event) -> {
            forgetPassword();
        });
        
        grid.add(new Label("Username:"), 0, 0);
        grid.add(username, 1, 0);
        grid.add(new Label("Password:"), 0, 1);
        grid.add(password, 1, 1);
        grid.add(lblfgtPass, 1, 2);

        dialog.getDialogPane().setContent(grid);

        // Request focus on the username field by default.
        Platform.runLater(() -> username.requestFocus());

        // Convert the result to a username-password-pair when the login button is clicked.
        dialog.setResultConverter(dialogButton -> {
            if (dialogButton == loginButtonType) {
                return new Pair<>(username.getText(), password.getText());
            }
            return null;
        });
        
        Optional<Pair<String, String>> result = dialog.showAndWait();
        
        successLogin = false;
        result.ifPresent(info -> {
            if(info.getKey().compareTo(this.usr)==0 && info.getValue().compareTo(pss)==0){
                successLogin = true;
            }else{
                System.out.println(">> wrong username/password");
                display();
            }
        });
        
        return successLogin;
    }
    
    private void forgetPassword(){
        Dialog<String> dialog = new Dialog<>();
        ButtonType btnConfirm = new ButtonType("Confirm", ButtonBar.ButtonData.OK_DONE);
        dialog.getDialogPane().getButtonTypes().addAll( btnConfirm, ButtonType.CANCEL);
        ((Stage)(dialog.getDialogPane().getScene().getWindow())).getIcons().add(Method.ImageFromFile("resource/icon.png"));
        
        PasswordField pass = new PasswordField();
        
        GridPane grid = new GridPane();
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(20, 150, 10, 10));
        
        grid.add(new Label("Secondary password: "), 0, 0);
        grid.add(pass, 1, 0);
        
        dialog.getDialogPane().setContent(grid);
        
        dialog.setResultConverter((btn) -> {
            if(btn == btnConfirm){
                return pass.getText();
            }
            return null;
        });
        
        Optional<String> res = dialog.showAndWait();
        
        if(res.isPresent() && res.get().compareTo(pss2)==0){ //  pass true
            resetPass();
        }else if(res.isPresent()){
            System.out.println("Wrong secondary password!");
        }
        
    }
    
    private void resetPass(){
        Dialog<String> dialog = new Dialog<>();
        ButtonType btnChange = new ButtonType("Change", ButtonBar.ButtonData.OK_DONE);
        dialog.getDialogPane().getButtonTypes().addAll( btnChange, ButtonType.CANCEL);
        ((Stage)(dialog.getDialogPane().getScene().getWindow())).getIcons().add(Method.ImageFromFile("resource/icon.png"));
        
        PasswordField pass = new PasswordField(),
                passCfr = new PasswordField();
        
        GridPane grid = new GridPane();
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(20, 150, 10, 10));
        
        grid.add(new Label("New password: "), 0, 0);
        grid.add(pass, 1, 0);
        grid.add(new Label("Re-type password: "), 0, 1);
        grid.add(passCfr, 1, 1);
        
        dialog.getDialogPane().setContent(grid);
        
        // Enable/Disable login button depending on whether a username was entered.
        Node changeButton = dialog.getDialogPane().lookupButton(btnChange);
        changeButton.setDisable(true);

        // Do some validation (using the Java 8 lambda syntax).
        pass.textProperty().addListener((observable, oldValue, newValue) -> {
            changeButton.setDisable(newValue.trim().isEmpty());
        });
        
        error = false;
        dialog.setResultConverter((btn) -> {
            if(btn == btnChange){
                if(pass.getText().length() < 3){
                    System.out.println(">> length of password must greater than 3 character!");
                    error = true;
                }else if(pass.getText().compareTo(passCfr.getText())==0){
                    System.out.println(">> password changed: " + pass.getText());
                    error = false;
                    return pass.getText();
                }else{
                    System.out.println(">> Wrong re-type password!");
                    error = true;
                }
            }
            return null;
        });
        
        String res = resultRegister(dialog);
        try{
            if(res != null){
                Statement s = con.createStatement();
                s.executeUpdate("update account set password='" + res + "' where username='" + usr + "'");
                pss = res;
            }
        }catch(SQLException ex){
            System.out.println(">> error in update password! ");
            ex.printStackTrace();
        }
    }
    
    private void registerAccount(){
        Dialog<List<String>> dialog = new Dialog<>();
        ((Stage)(dialog.getDialogPane().getScene().getWindow())).getIcons().add(Method.ImageFromFile("resource/icon.png"));
        ButtonType btnChange = new ButtonType("Register", ButtonBar.ButtonData.OK_DONE);
        dialog.getDialogPane().getButtonTypes().addAll( btnChange, ButtonType.CANCEL);
        
        TextField username = new TextField();
        PasswordField pass = new PasswordField(),
                passCfr = new PasswordField(),
                pass2 = new PasswordField();
        
        GridPane grid = new GridPane();
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(20, 150, 10, 10));
        
        
        grid.add(new Label("Username: "), 0, 0);
        grid.add(username, 1, 0);
        grid.add(new Label("Password: "), 0, 1);
        grid.add(pass, 1, 1);
        grid.add(new Label("Re-type password: "), 0, 2);
        grid.add(passCfr, 1, 2);
        grid.add(new Label("Secondary password: "), 0, 3);
        grid.add(pass2, 1, 3);
        
        dialog.getDialogPane().setContent(grid);
        
        // Enable/Disable login button depending on whether a username was entered.
        Node changeButton = dialog.getDialogPane().lookupButton(btnChange);
        changeButton.setDisable(true);

        // Do some validation (using the Java 8 lambda syntax).
        pass.textProperty().addListener((observable, oldValue, newValue) -> {
            changeButton.setDisable(newValue.trim().isEmpty());
        });
        
        error = false;
        dialog.setResultConverter((btn) -> {
            if(btn == btnChange){
                if(pass.getText().length() < 3){
                    System.out.println(">> length of password must greater than 3 character!");
                    error = true;
                }else if(pass.getText().compareTo(passCfr.getText())==0){
                    LinkedList<String> list = new LinkedList<>();
                    list.add(username.getText());
                    list.add(pass.getText());
                    list.add(pass2.getText());
                    error = false;
                    return list;
                }else{
                    System.out.println(">> Wrong re-type password!");
                    error = true;
                }
            }
            return null;
        });
        
        try{
            List<String> res = resultRegister(dialog);
            Statement s = con.createStatement();
            s.executeUpdate("insert into account values ('" + res.get(0) + "', '" + res.get(1) + "', '" + res.get(2) + "')");
            usr = res.get(0); pss = res.get(1); pss2 = res.get(2);
            display();
        }catch(SQLException ex){
            System.out.println(">> error in update password! ");
            ex.printStackTrace();
        }
    }
    
    private <T> T resultRegister(Dialog<T> dialog){
        Optional<T> res = dialog.showAndWait();
        
        if(res.isPresent()){
            System.out.println("result " + res.get());
            return res.get();
        }else if(error){
            return resultRegister(dialog);
        }
        return null;
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        con.close();
    }
}