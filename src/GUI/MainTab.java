package GUI;

import javafx.scene.control.Button;

/**
 *
 * @author nmhillusion
 */
public class MainTab extends Button{
    private boolean selected;
    
    public MainTab(String title) {
        super(title);
        getStyleClass().add("main-tab");
    }
    
    
    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
        if(selected){
            getStyleClass().add("main-tab-selected");
        }else{
            getStyleClass().remove("main-tab-selected");
        }
    }
}