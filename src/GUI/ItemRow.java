package GUI;

import ActionDialog.EditListDialog;
import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedHashMap;
import javafx.geometry.Pos;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.HBox;
import javafx.scene.text.TextAlignment;
import managedocumentoflecturer.Method;
import ActionDialog.ModelFormAction;
import java.util.Arrays;
import java.util.List;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.layout.StackPane;
import resourceClass.ObjectModel;

/**
 * created 4/21/17
 * @author nmhillusion
 */
public class ItemRow extends HBox{
    private LinkedHashMap<String, Object> data;
    private static LinkedHashMap<String, Integer> staticWidthColumn;
    private LinkedHashMap<String, Integer> widthColumn = new LinkedHashMap<>();
    private ContextMenu contextMenu;
    private static Class staticCls;
    private Class clientClass;
    private static ClassTab staticClsTab;
    private ClassTab container;
    private boolean selected = false;
    private boolean selectMulty = false;
    
    public ItemRow(LinkedHashMap<String, Integer> header, Class _cls, ClassTab _container) {
        super();
        staticWidthColumn = new LinkedHashMap<>();
        header.forEach((t, u) -> {
            getChildren().add(new hLabel(t, u, _container, ClassTab.getMainTab()));
            staticWidthColumn.put(t.toLowerCase().replaceAll(" ", ""), u);
        });
        setAlignment(Pos.CENTER);
        staticCls = _cls;
        staticClsTab = _container;
    }
    
    public ItemRow(LinkedHashMap<String, Object> input, String maintab) {
        super();
        data = input;
        clientClass = staticCls;
        container = staticClsTab;
        widthColumn = staticWidthColumn;
        
        setRow();
        setAlignment(Pos.CENTER);
        getStyleClass().add("i-row");
        
        
        setOnMouseClicked((event) -> {
            if(event.getClickCount() == 2){
                //  to-do when double click
            }
            if(!selectMulty || !event.isControlDown()){
                container.unSelectItem();
            }
            if(!selected){
                setSelected();
            }else if(event.isControlDown()){
                unSelected();
            }
        });
        
        contextMenu = createContextMenu(maintab);
        
        setOnContextMenuRequested((event) -> {
            setSelected();
            if(contextMenu!=null)contextMenu.show(this, event.getScreenX() + 5, event.getScreenY());
        });
        
        if(contextMenu!=null)contextMenu.setOnHidden((event) -> {
            unSelected();
        });
        
    }
    
    private void setRow(){
        ObjectModel object = null;
        try {
            object = (ObjectModel)clientClass.newInstance();
            object.setPropertise(data);
            getChildren().clear();
            object.getProperties().forEach((t, u) -> {
                if (u instanceof Boolean) {
                    CheckBox chk = new CheckBox();
                    chk.setSelected(Boolean.valueOf(u.toString()));
                    chk.setDisable(true);
                    Object widthCell = widthColumn.get(t.replaceAll(" ", "").toLowerCase());
                    if(widthCell != null && (int)widthCell > 0){
                        StackPane c = new StackPane(chk); c.getStyleClass().add("i-label");
                        c.setPrefSize((int)widthCell, 25);
                        getChildren().add(c);
                    }
                }else if (Method.parse2List(u.toString()).size() > 0) {
                    LinkedHashMap<String, Object> parse = Method.parse2List(u.toString());
                    if(parse.get("type").toString().compareTo("single")==0){
                        Object widthCell = widthColumn.get(t.replaceAll(" ", "").toLowerCase());
                        if(widthCell != null && (int)widthCell > 0)getChildren().add(new iLabel(parse.get("value-display").toString(), (int)widthCell));
                    }else if(parse.get("type").toString().compareTo("list")==0){
                        ComboBox<String> list = new ComboBox<>();
                        list.getItems().addAll((List<String>)parse.get("value-display"));
                        list.getSelectionModel().select(0);
                        Object widthCell = widthColumn.get(t.replaceAll(" ", "").toLowerCase());
                        if(widthCell != null && (int)widthCell > 0){
                            list.setPrefSize((int) widthCell, 25); list.getStyleClass().add("i-label");
                            getChildren().add(list);
                        }
                    }
                }else{
                    Object widthCell = widthColumn.get(t.replaceAll(" ", "").toLowerCase());
                    if(widthCell != null && (int)widthCell > 0)getChildren().add(new iLabel(u.toString().trim(), (int)widthCell));
                }
            });

        } catch (IllegalAccessException | InstantiationException ex) {
            System.out.println("// error in itemrow: " + ex.getMessage());
        }
    }
    
    public ItemRow setSelectMulty(boolean val){
        selectMulty = val;
        return this;
    }
    
    private ContextMenu createContextMenu(String maintab){
        if(maintab.compareToIgnoreCase("select")==0) return null;
        else if(maintab.compareToIgnoreCase("recycle bin") == 0) return createContextMenuRecycleBin(maintab);
        else if(maintab.compareToIgnoreCase("editList") == 0) return createContextMenuEditList(maintab);
        
        MenuItem edit = new MenuItem("Edit"),
                view = new MenuItem("view"),
                delete = new MenuItem("Delete");
        
        view.setOnAction((event) -> {
            System.out.println(" do something....");
        });
        
        edit.setOnAction((event) -> {
            ObjectModel object = null;
            try {
                object = (ObjectModel)clientClass.newInstance();
                object.setPropertise(data);
                System.out.println(" edit " + clientClass.getSimpleName());
                ModelFormAction m = new ModelFormAction<>("Edit in " + clientClass.getSimpleName(), object.getDeepKeys(), object);
                int type = 0;   //  0 : false, 1: true, 2: false
                while(type==0){
                    switch(m.display().get().toString()){
                        case "true": type = 1;break;
                        case "false": type = 0;break;
                        case "cancel": type = 2;break;
                    }
                    System.out.println("res: " + type);
                }
                
                if(type==1){
                    //  edit this row
                    data = object.getProperties();
                    object.editDatabase();
                    setRow();
                }
            } catch (IllegalAccessException | InstantiationException ex) {
                System.out.println("// error in create context menu itemrow: " + ex.getMessage());
                ex.printStackTrace();
            }
        });
        
        delete.setOnAction((event) -> {
            ObjectModel object = null;
            try {
                object = (ObjectModel)clientClass.newInstance();
                object.setPropertise(data);
                
                if(object.canDelete() && clientClass.getSimpleName().compareToIgnoreCase("Score")!=0){
                    Connection con = Method.getConnection();
                    Statement s = con.createStatement();
                    int res = s.executeUpdate("update " + clientClass.getSimpleName() +" set isLive='False' where ID='" + getID() + "'");

                    if(res!=0){
                        System.out.println("deleted");
                    }
                    con.close();
                    container.setContent(maintab);
                }else{
                    object.deleteDatabase();
                    container.setContent(maintab);
                }
            } catch (SQLException | InstantiationException |IllegalAccessException e) {
                System.out.println("error in class item row: delete manage");
                e.printStackTrace();
            }
        });
        
        ContextMenu ctx = new ContextMenu( view, edit,delete);
        if(data.containsKey("path") || data.containsKey("Path")){
            MenuItem open = new MenuItem("open");
            open.setOnAction((event) -> {
                String path = "";
                if(data.containsKey("path")){
                    path = data.get("path").toString();
                }else{
                    path = data.get("Path").toString();
                }
                if(new File(path).exists())try {
                    Desktop.getDesktop().open(new File(path));
                } catch (IOException ex) {
                    System.out.println(">> " + path + " is not exist!");
                }        
            });
            
            ctx.getItems().add(0, open);
        }
        return ctx;
    }
    
    
    private ContextMenu createContextMenuRecycleBin(String maintab){
        MenuItem restore = new MenuItem("restore"),
                delete = new MenuItem("permanent delete");
        
        restore.setOnAction((event) -> {
            try {
                if(clientClass.getSimpleName().compareToIgnoreCase("studentgroup")!=0){
                    try (Connection con = Method.getConnection()) {
                        Statement s = con.createStatement();
                        int res = s.executeUpdate("update " + clientClass.getSimpleName() +" set isLive='True' where ID='" + getID() + "'");
                        
                        if(res!=0){
                            System.out.println("restored");
                        }
                    }
                    container.setContent(maintab);
                }else{
                    System.out.println("Can not restore group, please restore the project linking with it!");
                }
            } catch (SQLException e) {
                System.out.println("error in class item row: restore manage");
            }
        });
        
        delete.setOnAction((event) -> {
            ObjectModel object = null;
            try {
                object = (ObjectModel)clientClass.newInstance();
                object.setPropertise(data);
                object.deleteDatabase();
                
                File f = null;
                if(data.containsKey("path")){
                    f = new File(data.get("path").toString());
                }else if(data.containsKey("Path")){
                    f = new File(data.get("Path").toString());
                }
                if(!f.delete()){
                    f.deleteOnExit();
                }
                
                System.out.println("deleted permanently...");
                container.setContent(maintab);
            } catch (IllegalAccessException | InstantiationException e) {
                System.out.println("error in class item row: delete permanently manage");
            }
        });
        
        return new ContextMenu(restore, delete);
    }
    
    private ContextMenu createContextMenuEditList(String maintab){
        MenuItem delete = new MenuItem("delete");
        delete.setOnAction((event) -> {
            if(editdialog != null){
                editdialog.deleteItems(Arrays.asList(new String[]{getID()}));
            }
        });
        return new ContextMenu(delete);
    }
    
    private static EditListDialog editdialog;
    public static void setEditListDialog(EditListDialog dialog){
        editdialog = dialog;
    }
    
    public String getID(){
        return data.get("ID").toString().trim();
    }
    
    public boolean isSelected(){
        return selected;
    }
    
    private void setSelected(){
        setId("i-row-selected");
        selected = true;
    }
    
    public void unSelected(){
        setId("i-row-nonselected");
        selected = false;
    }
}

class iLabel extends Label{
    private static int width = 120;
    public iLabel(String txt) {
        super(txt);
        getStyleClass().add("i-label");
        setTooltip(new Tooltip(txt));
        setPrefWidth(width);
    }
    
    public iLabel(String txt, int _width) {
        super(txt);
        getStyleClass().add("i-label");
        setTooltip(new Tooltip(txt));
        setPrefWidth(_width);
        setTextAlignment(TextAlignment.CENTER);
    }
}

class hLabel extends Label{
    public hLabel(String txt, int _width, ClassTab tab, String maintab) {
        super(txt);
        getStyleClass().add("h-label");
        setTooltip(new Tooltip(txt));
        setPrefWidth(_width);
        setTextAlignment(TextAlignment.CENTER);
        
        setOnMousePressed((event) -> {
            tab.setSortBy(txt);
            tab.setContent(maintab);
        });
    }
}