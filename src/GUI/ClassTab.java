package GUI;

import ActionDialog.EditListDialog;
import managedocumentoflecturer.Search;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Tab;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import managedocumentoflecturer.ManageDocumentOfLecturer;
import managedocumentoflecturer.Method;
import ActionDialog.ModelFormAction;
import java.sql.ResultSetMetaData;
import java.util.ArrayList;
import javafx.scene.input.KeyCode;
import resourceClass.ObjectModel;
import managedocumentoflecturer.Statistic;

/**
 *
 * @author nmhillusion
 */
public class ClassTab extends Tab{
    private ComboBox<String> cboField;
    private TextField inputSearch;
    private Button btnSearchAdven;
    private VBox content, vboxTableContent;
    private VBox table;
    private GridPane advanBox;
    private Class thisClass;
    private ResultSet rs;
    private LinkedHashMap<String, Integer> header = new LinkedHashMap<>();
    private LinkedHashMap<String, Object> listAdvanSch = new LinkedHashMap<>();
    private short idxAdvanBox= 0, rows = -1;
    private Search sea=new Search();
    private List<ItemRow> listSelectedItem = new LinkedList<ItemRow>();
    private final String title;
    private static ManageDocumentOfLecturer mainprogram;
    private boolean isMultySelect = false;
    private final ScrollPane contentOfTab = new ScrollPane();
    private String sortBy = "ID";
    
    public ClassTab(String txt){
        super(txt);
        setId("class-tab");
        setClosable(false);
        
        title = txt;
        advanBox = new GridPane();
        
        try {
            thisClass = Class.forName("resourceClass." + title.replaceAll(" ", ""));
            cboField = new ComboBox<>();
        } catch (ClassNotFoundException ex) {
            System.out.println("can not find class : " + txt);
        }
        
        setOnSelectionChanged((event) -> {
            if(mainprogram!=null && isSelected()){
                setContent(mainprogram.getMainTabSelected());
//                mainprogram.setStatus(getMainTab() + " > " + txt);
            }
        });
    }
    
    public static void setMainProgram(ManageDocumentOfLecturer main){
        mainprogram = main;
    }
    
    public static String getMainTab(){
        return mainprogram.getMainTabSelected();
    }
    
    public void setSortBy(String field){
        sortBy = field.trim().toLowerCase().replaceAll(" ", "");
    }
    
    /**
     * Cài đặt nội dung cho tab này, tùy thuộc vào maintab đang chọn
     * @param maintab main tab nào đang được chọn
     */
    public void setContent(String maintab){
        if(maintab.compareToIgnoreCase("statistic")!=0){  //  không phải là tab thống kê
            cboField.getItems().clear();
            idxAdvanBox = 0; rows = -1;
            
            if(vboxTableContent!=null)vboxTableContent.getChildren().clear();
            try {
                ObjectModel instance = (ObjectModel)thisClass.newInstance();
                instance.getWidthCell().keySet().forEach((t) -> {
                    cboField.getItems().add(t);
                });
            }catch (IllegalAccessException | InstantiationException ex) {
                System.out.println("// error in classtab get field combobox");
            }
            cboField.getSelectionModel().select(0);
            cboField.setPrefSize(100, 20);
            //  Setting for input field
            inputSearch = new TextField();
            inputSearch.setTooltip(new Tooltip("input keyword here and press enter..."));
            inputSearch.setPrefSize(530, 20);
            inputSearch.setOnKeyReleased((event) -> {
                
                String type;
                Object keyword= inputSearch.getText();
//                try {
//                    try{
//                        type = thisClass.getDeclaredField(cboField.getValue()).getType().toString();
//                    }catch(Exception ex){
//                        type = thisClass.getSuperclass().getDeclaredField(cboField.getValue()).getType().toString();
//                    }
//                    if(type.compareTo("boolean")==0){
//                        if(keyword.toString().compareToIgnoreCase("true")==0 || keyword.toString().compareToIgnoreCase("false")==0 ||
//                                keyword.toString().compareToIgnoreCase("1")==0 || keyword.toString().compareToIgnoreCase("0")==0){
//                            keyword = Boolean.valueOf(inputSearch.getText());
//                        }else{
//                            keyword = "";
//                        }
//                    }
//                } catch (NoSuchFieldException | SecurityException ex) {
//                    ex.printStackTrace();
//                }
                displayResultSearch(false, keyword);
            });
            //  Setting for advanced Search
            btnSearchAdven = new Button("advenced search");
            btnSearchAdven.setId("btnSearchAdven");
            advanBox.getChildren().clear();
            advanBox.setAlignment(Pos.CENTER);
            advanBox.setVgap(10);
            advanBox.setHgap(10);
            advanBox.setPadding(new Insets(10));
            cboField.getItems().forEach((t) -> {
                TextField txtfld = new TextField();
                advanBox.add(new Label(t), 0, idxAdvanBox);
                advanBox.add(txtfld, 1, idxAdvanBox++);
                listAdvanSch.put(t, txtfld);
            });
            Button btnSearch = new Button("Search"),
                    btnClose = new Button("Close");
            btnSearch.setOnAction((event) -> {                
                LinkedHashMap<String, Object> keyword = new LinkedHashMap<>();
                listAdvanSch.forEach((t, u) -> {
                    String val = ((TextField)u).getText();
                    if(val.compareTo("")!=0){
                        keyword.put(t, ((TextField)u).getText());
                    }
                    
                });
                
                displayResultSearch(true ,keyword);
            });
            btnClose.setOnAction((event) -> {
                vboxTableContent.getChildren().remove(advanBox);
                //      Disable normal search
                inputSearch.setDisable(false);
                btnSearchAdven.setDisable(false);
            });
            advanBox.add(btnSearch, 0, idxAdvanBox);
            advanBox.add(btnClose, 1, idxAdvanBox);
            advanBox.setMaxSize(600, 600);
            btnSearchAdven.setOnAction((event) -> {
                //      Disable normal search
                inputSearch.setDisable(true);
                btnSearchAdven.setDisable(true);
                displayResultSearch(false, "");
                vboxTableContent.getChildren().add(0, advanBox);
            });
            
            //      ===============================         BUILD TAB INIT      ============================================
            table = new VBox();
            table.setAlignment(Pos.CENTER);
            table.setMinWidth(750);
            System.out.println("==================> create tab " + thisClass.getSimpleName());
            try {
                header = ((ObjectModel)thisClass.newInstance()).getWidthCell();
                table.getChildren().add(new ItemRow(header, thisClass, this));
            } catch (IllegalAccessException | InstantiationException ex) {
                System.out.println("// error in create classtab: " + ex.getMessage());
                ex.printStackTrace();
            }
            
            try{
                try (Connection con = Method.getConnection()) {
                    Statement s = con.createStatement();
                    ResultSet _rs = (maintab.compareToIgnoreCase("recycle bin")!=0)?
                            s.executeQuery("select * from "+thisClass.getSimpleName() + " where isLive='True' order by " + sortBy):
                            s.executeQuery("select * from "+thisClass.getSimpleName() + " where isLive='False' order by " + sortBy);

                    while(_rs.next()){
                        LinkedHashMap<String, Object> input = new LinkedHashMap<>();
                        Arrays.asList(thisClass.getDeclaredFields()).forEach((t) -> {
                            try {
                                input.put(t.getName(), _rs.getObject(t.getName()));
                            } catch (SQLException ex) {
                                System.out.println(">> error in classtab, get data! field: " + t);
                            }
                        });

                        Arrays.asList(thisClass.getSuperclass().getDeclaredFields()).forEach((t) -> {
                            try {
                                input.put(t.getName(), _rs.getObject(t.getName()));
                            } catch (SQLException ex) {
                                System.out.println(">> error in classtab, get data! field: " + t);
                            }
                        });
                        table.getChildren().add(new ItemRow(input, maintab).setSelectMulty(isMultySelect));
                    }
                    con.close();
                }
            }catch(SQLException ex){
                System.out.println(">> error in classtab, get data! " + ex.getMessage());
            }
            
            vboxTableContent = new VBox(table);
            ScrollPane contentTable = new ScrollPane(vboxTableContent);
            contentTable.setPrefSize(630, 400);
            content = new VBox(5, new HBox(8, cboField, inputSearch, btnSearchAdven), contentTable);
            if(maintab.matches("(?i)manage|(?i)select")){
                Button addNew = new Button("Add New");
                addNew.setOnAction((event) -> {
                    try {
                        ObjectModel object = (ObjectModel) thisClass.newInstance();
                        System.out.println("new instance: " + object.getProperties());
                        ModelFormAction m = new ModelFormAction<>("Add new " + thisClass.getSimpleName(), object.getDeepKeys(), object);
                        int type = 0;   //  0 : false, 1: true, 2: false
                        while(type==0){
                            switch(m.display().get().toString()){
                                case "true": type = 1;break;
                                case "false": type = 0;break;
                                case "cancel": type = 2;break;
                            }
                        }

                        if(type==1){
                            object.addToDatabase();
                            setContent(maintab);
                        }
                    } catch (IllegalAccessException | InstantiationException ex) {
                        ex.printStackTrace();
                    }
                });
                
                content.getChildren().add(addNew);
            }
            content.setId("class-tab-content");
            contentOfTab.setContent(content);
            setContent(contentOfTab);
            
        }else{      //  tab thống kê
            LinkedHashMap<String, String> res = Statistic.exec(thisClass.getSimpleName());
            GridPane scontent = new GridPane();
            scontent.setPadding(new Insets(20));
            scontent.setVgap(10); scontent.setHgap(20);
            
            class sLabel extends Label{

                public sLabel(String txt, boolean header) {
                    super(txt);
                    if(header){
                        getStyleClass().add("hlabel-statistic");
                    }else{
                        getStyleClass().add("ilabel-statistic");
                    }
                }
            }
            
            rows  = -1;
            res.forEach((t, u) -> {
                scontent.add(new sLabel(t, true), 0, ++rows, 3, 1);
                try {
                    try (Connection con = Method.getConnection()) {
                        Statement s = con.createStatement();
                        ResultSet irs = s.executeQuery(u);
                        ResultSetMetaData rsm = irs.getMetaData();
                        int maxCnt = rsm.getColumnCount();
                        
                        while(irs.next()){
                            ++rows;
                            for(int i = 0; i < maxCnt; ++i){
                                scontent.add(new sLabel(irs.getString(rsm.getColumnName(i+1)).trim(), false), i+1, rows, 1, 1);
                            }
                        }
                    }
                } catch (SQLException ex) {
                    System.out.println("//  error in statistic tab: " + ex.getLocalizedMessage());
                    ex.printStackTrace();
                }
                
            });
            if(res != null && res.size() > 0){
                contentOfTab.setContent(scontent);
            }else{
                sLabel notify = new sLabel("Nothing to statistic", true);
                notify.setStyle("-fx-text-size: 30; -fx-font-style: ITALIC");
                notify.setPrefWidth(600);
                contentOfTab.setContent(notify);
                contentOfTab.setPadding(new Insets(20));
            }
            setContent(contentOfTab);
        }
    }
    
    private void displayResultSearch(boolean advanSea, Object keyword){
        rs= (advanSea)?sea.advancedsearch(thisClass.getSimpleName(), (LinkedHashMap<String, Object>)keyword):
                sea.normalsearch(thisClass.getSimpleName(), cboField.getValue(), keyword);
        
        table.getChildren().clear();
        table.getChildren().add(new ItemRow(header, thisClass,this));
        try {
            while(rs.next()){
                LinkedHashMap<String, Object> input = new LinkedHashMap<>();
                cboField.getItems().forEach((t) -> {
                    try {
                        input.put(t, rs.getObject(t.trim().toLowerCase().replaceAll(" ", "")).toString().trim());
                    } catch (SQLException ex) {
                        System.out.println(">> error in classtab, get data! field: " + t);
                    }
                });
                table.getChildren().add(new ItemRow(input, getMainTab()).setSelectMulty(isMultySelect));
            }
        } catch (SQLException ex) {

        }
    }
    
    public void setContentEditList(String maintab, List<String> inputID, EditListDialog dialog){
        table = new VBox();
        table.setAlignment(Pos.CENTER);
        table.setMinWidth(750);
        
        ItemRow.setEditListDialog(dialog);
        
        try {
            header = ((ObjectModel)thisClass.newInstance()).getWidthCell();
            table.getChildren().add(new ItemRow(header, thisClass, this));
        } catch (Exception ex) {
            System.out.println("// error in create classtab: " + ex.getMessage());
            ex.printStackTrace();
        }
        
        try{
            try (Connection con = Method.getConnection()) {
                Statement s = con.createStatement();
                inputID.forEach((idRow) -> {
                    try{
                        ResultSet _rs = s.executeQuery("select * from "+thisClass.getSimpleName() + " where isLive='True' and ID='" + idRow + "'");

                        if(_rs.next()){
                            LinkedHashMap<String, Object> input = new LinkedHashMap<>();
                            Arrays.asList(thisClass.getDeclaredFields()).forEach((t) -> {
                                try {
                                    input.put(t.getName(), _rs.getObject(t.getName()));
                                } catch (SQLException ex) {
                                    System.out.println(">> error in classtab, get data! field: " + t);
                                }
                            });

                            Arrays.asList(thisClass.getSuperclass().getDeclaredFields()).forEach((t) -> {
                                try {
                                    input.put(t.getName(), _rs.getObject(t.getName()));
                                } catch (SQLException ex) {
                                    System.out.println(">> error in classtab, get data! field: " + t);
                                }
                            });
                            table.getChildren().add(new ItemRow(input, maintab).setSelectMulty(isMultySelect));
                        }
                    }catch(SQLException ex){
                        System.out.println(">> error in classtab, get data item editlist! " + ex.getMessage());
                    }
                });
                con.close();
            }
        }catch(SQLException ex){
            System.out.println(">> error in classtab, get data! " + ex.getMessage());
        }
        
        vboxTableContent = new VBox(table);
        ScrollPane contentTable = new ScrollPane(vboxTableContent);
        contentTable.setPrefSize(630, 400);
        content = new VBox(contentTable);
        content.setId("class-tab-content");
        contentOfTab.setContent(content);
        setContent(contentOfTab);
        
        contentOfTab.setOnKeyPressed((event) -> {
            if(dialog != null && event.getCode() == KeyCode.DELETE){
                System.out.println("i want to delete: " + getSelectedIDs());
                dialog.deleteItems(getSelectedIDs());
            }
        });
    }
    
    public ClassTab setMultySelect(boolean val){
        isMultySelect = val;
        return this;
    }
    
    public void unSelectItem(){
        table.getChildren().forEach((t) -> {
            if(((ItemRow)t).isSelected()){
                ((ItemRow)t).unSelected();
            }
        });
    }
    
    public List<ItemRow> getSelectedItems(){
        listSelectedItem.clear();
        table.getChildren().forEach((t) -> {
            if(((ItemRow)t).isSelected()){
                listSelectedItem.add(((ItemRow)t));
            }
        });
        
        return listSelectedItem;
    }
    
    public List<String> getSelectedIDs(){
        List<String> res = new ArrayList<>();
        getSelectedItems().forEach((t) -> {
            res.add(t.getID());
        });
        return res;
    }
}