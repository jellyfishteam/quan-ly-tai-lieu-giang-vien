package resourceClass;

import java.io.File;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedHashMap;
import managedocumentoflecturer.Method;

/**
 *
 * @author nmhillusion
 */
abstract class ProjectStudent implements ObjectModel{
    private String ID;
    private String name;
    private int semester;
    private String schoolYear;
    private String instructor;
    private String examiner;
    private String topicID;
    private String subjectID;
    private String groupID;
    private double tenScore;
    private String wordScore;
    private boolean isValid, isLive;
    private String path;

    public ProjectStudent(){
        this.ID = createID();
        this.name = "";
        this.semester = 1;
        this.schoolYear = "0-1";
        this.instructor = ":fk-Instructor:[]";
        this.examiner = ":fk-Examiner:()";
        this.topicID = ":fk-Topic:()";
        this.subjectID = ":fk-Subject:()";
        this.groupID = ":fk-StudentGroup:()";
        this.tenScore = 0;
        this.wordScore = "B";
        this.isLive = false;
        this.path = "";
    }
    
    abstract protected String createID();
    
    public ProjectStudent(String ID, String name, int semester, String schoolYear, String instructors, String examiners, String topicID, String subjectID, float tenScore, String wordScore, boolean isLive,  String path) {
        this.ID = ID;
        this.name = name;
        this.semester = semester;
        this.schoolYear = schoolYear;
        this.instructor = instructors;
        this.examiner = examiners;
        this.topicID = topicID;
        this.subjectID = subjectID;
        this.tenScore = tenScore;
        this.wordScore = wordScore;
        this.isLive = isLive;
        this.path = path;
    }
    
    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getSemester() {
        return semester;
    }

    public void setSemester(int semester) {
        this.semester = semester;
    }

    public String getSchoolYear() {
        return schoolYear;
    }

    public void setSchoolYear(String schoolYear) {
        this.schoolYear = schoolYear;
    }

    public String getInstructor() {
        return instructor;
    }

    public void setInstructor(String instructor) {
        this.instructor = instructor;
    }

    public String getExaminer() {
        return examiner;
    }

    public void setExaminer(String examiner) {
        this.examiner = examiner;
    }

  

    public String getTopicID() {
        return topicID;
    }

    public void setTopicID(String topicID) {
        this.topicID = topicID;
    }

    public String getSubjectID() {
        return subjectID;
    }

    public void setSubjectID(String subjectID) {
        this.subjectID = subjectID;
    }

    public double getTenScore() {
        return tenScore;
    }

    public void setTenScore(double tenScore) {
        this.tenScore = tenScore;
    }

    public String getWordScore() {
        return wordScore;
    }

    public void setWordScore(String wordScore) {
        this.wordScore = wordScore;
    }

    public boolean isValid() {
        return isValid;
    }

    public void setIsValid(boolean isValid) {
        this.isValid = isValid;
    }

    public boolean isIsLive() {
        return isLive;
    }

    public void setIsLive(boolean isLive) {
        this.isLive = isLive;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        if(this.path.length() > 1){
            File f = new File(this.path);
            if(!f.delete()){
                f.deleteOnExit();
            }
        }
        
        this.path = Method.moveFile(path, "DataUser//ProjectStudent//" + getClass().getSimpleName());
    }

    public String getGroupID() {
        return groupID;
    }

    public void setGroupID(String groupID) {
        this.groupID = groupID;
    }

    @Override
    public LinkedHashMap<String, Object> getProperties() {
        LinkedHashMap<String, Object> res = new LinkedHashMap<>();
        
        res.put("ID", getID());
        res.put("Name", getName());
        res.put("Semester", getSemester());
        res.put("School Year", getSchoolYear());
        res.put("Instructor", getInstructor());
        res.put("Examiner", getExaminer());
        res.put("Topic ID", getTopicID());
        res.put("Group ID", getGroupID());
        res.put("Subject ID", getSubjectID());
        res.put("Ten Score", getTenScore());
        res.put("Word Score", getWordScore());
        res.put("Path", getPath());
        
        return res;
    }
    
    @Override
    public String[] getDeepKeys() {
        return new String[]{"ID", "Word Score"};
    }

    @Override
    public void setPropertise(LinkedHashMap<String, Object> value) {
        System.out.println("set value: " + value);
        value.forEach((k, val) -> {
            String v = val.toString().trim();
            switch(k.trim().replaceAll(" ", "").toLowerCase()){
                case "id": setID(v); break;
                case "name":  setName(v); break;
                case "semester":  setSemester(Integer.valueOf(v)); break;
                case "schoolyear":  setSchoolYear(v); break;
                case "instructor": setInstructor(v); break;
                case "examiner": setExaminer(v); break;
                case "topicid": setTopicID(v); break;
                case "subjectid": setSubjectID(v); break;
                case "groupid": setGroupID(v); break;
                case "tenscore":  setTenScore(Double.valueOf(v)); break;
                case "path": setPath(v); break;
            }
        });
        
        setWordScore(Method.switchScore(tenScore).get(0).toString());
    }

    @Override
    public boolean validateValue(LinkedHashMap<String, Object> value) {
        setIsValid(true);
        value.forEach((k, v) -> {
            if(ObjectModel.isNullString(v.toString())) setIsValid(false);
            else switch(k.trim().replaceAll(" ", "").toLowerCase()){
                case "semester":
                    try{
                        int val = Integer.valueOf(v.toString());
                        if( 1 > val || val > 3) throw new NumberFormatException();
                    }catch(NumberFormatException ex){
                        Method.Dialog(Method.DialogType.ERROR, "Error in input", ">> Semester is not available.");
                        setIsValid(false);
                    }
                    break;
                case "schoolyear":
                    if(!v.toString().matches("\\d+-\\d+")){
                        Method.Dialog(Method.DialogType.ERROR, "Error in input", ">> " +k +" is not available.");
                        setIsValid(false);
                    }
                    break;
                case "tenscore":  
                    try{
                        double val = Double.valueOf(v.toString());
                        if(0 > val || val > 10){
                            throw new NumberFormatException();
                        }
                    }catch(NumberFormatException ex){
                        Method.Dialog(Method.DialogType.ERROR, "Error in input", ">> Score is not available.");
                        setIsValid(false);
                    }
                    break;
                    
                case "groupid":
                case "subjectid":
                case "examiner":
                case "instructor":
                case "topicid":
                    if(Method.parse2List(v.toString()).isEmpty()){
                        Method.Dialog(Method.DialogType.ERROR, "Error in input", ">> "+k+" is not available.");
                        setIsValid(false);
                    }
            }
        });
        return isValid;
    }

    @Override
    public LinkedHashMap<String, Integer> getWidthCell() {
        LinkedHashMap<String, Integer> res = new LinkedHashMap<>();
        
            res.put("ID", 60);
            res.put("Name", 220);
            res.put("Semester", 100);
            res.put("School Year", 180);
            res.put("Instructor", 200);
            res.put("Examiner", 200);
            res.put("Topic ID", 100);
            res.put("Group ID", 100);
            res.put("Subject ID", 100);
            res.put("Ten Score", 100);
            res.put("Word Score", 100);
            res.put("Path", 250);
            
        return res;
    }
    
    @Override
    public void addToDatabase(){
        try {
            Connection con = Method.getConnection();
            Statement s = con.createStatement();
            
            s.execute("insert into " + getClass().getSimpleName() + " (ID, name, semester, schoolYear, instructor, examiner, topicID, groupID, subjectID, tenScore, wordScore, path, isLive)"
                      + " values ('"+ID+"', '"+name+"', "+semester+", '"+schoolYear+"', '"+instructor+"', '"+examiner+"', '"+topicID+"', '"+groupID+"', '"+subjectID+"', "+tenScore+", '"+wordScore+"', '"+path+"', 'True')");
            
            con.close();
        } catch (SQLException e) {
            System.out.println("//  error in add database of projectstudent");
        }
    }
    
    @Override
    public void editDatabase(){
        try {
            Connection con = Method.getConnection();
            Statement s = con.createStatement();
            
            s.execute("update " + getClass().getSimpleName()
                      + " set name='"+name+"', semester="+semester+", schoolYear='"+schoolYear+"', instructor='"+instructor+"', examiner='"+examiner+"',"
                              + " topicID='"+topicID+"', groupID='"+groupID+"', subjectID='"+subjectID+"', tenScore="+tenScore+", wordScore='"+wordScore+"', path='"+path+"'"
                      + " where ID='" + ID + "'"
            );
            
            con.close();
        } catch (SQLException e) {
            System.out.println("//  error in edit of projectstudent");
        }
    }
    
    @Override
    public boolean canDelete(){
        return true;
    }
    
    @Override
    public void deleteDatabase(){
        try {
            Connection con = Method.getConnection();
            Statement s = con.createStatement();
            
            s.execute("delete from " + getClass().getSimpleName()
                      + " where ID='" + ID + "'"
            );
            
            s.execute("delete from StudentGroup where projectID='" + ID + "'");
            
            con.close();
        } catch (SQLException e) {
            System.out.println("//  error in delete of projectstudent");
        }
    }
}