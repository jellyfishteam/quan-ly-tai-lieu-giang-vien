
package resourceClass;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedHashMap;
import managedocumentoflecturer.Method;

public class Exercise extends TeachingMaterial{
    
    public Exercise(){
        super();
    }
    
    @Override
    protected String createID() {
        String query = "select top 1 ID from "+getClass().getSimpleName()+" order by ID desc";
        short serial = 0;
        try {
            Connection con = Method.getConnection();
            Statement s = con.createStatement();
            ResultSet rs = s.executeQuery(query);
            if(rs.next()){
                String oldID = rs.getString("ID");
                serial = Short.valueOf(oldID.substring(oldID.lastIndexOf("-")+1).trim());
            }
        } catch (SQLException e) {
            System.out.println("// error in create ID");
        }
        
        return "BT-" + (serial+1);
    }

    @Override
    public LinkedHashMap<String, Object> getProperties() {
        LinkedHashMap<String, Object> res = new LinkedHashMap<>();
        
        res.put("ID", getID());
        res.put("Subject ID", getSubjectID());
        res.put("Path", getPath());
        
        return res;
    }

    @Override
    public String[] getDeepKeys() {
        return new String[]{"ID"};
    }

    @Override
    public void setPropertise(LinkedHashMap<String, Object> value) {
        value.forEach((String k, Object v) -> {
            switch (k.trim().replaceAll(" ", "").toLowerCase()) {
                case "id":
                    setID(v.toString());
                    break;
                case "subjectid":
                    setSubjectID(v.toString());
                    break;
                case "path":
                    setPath(v.toString());
                    break;
            }
        });
        
    }

    @Override
    public boolean validateValue(LinkedHashMap<String, Object> value) {
        setIsValid(true);
        value.forEach((k, v) -> {
            if(ObjectModel.isNullString(v.toString())) setIsValid(false);
            else switch(k.trim().replaceAll(" ", "").toLowerCase()){
                case "subjectid":
                    if(Method.parse2List(v.toString()).isEmpty()){
                        Method.Dialog(Method.DialogType.ERROR, "Error in input", ">> "+k+" is not available.");
                        setIsValid(false);
                    }
            }
        });
        return isValid();
    }

    @Override
    public LinkedHashMap<String, Integer> getWidthCell() {
        LinkedHashMap<String, Integer> res = new LinkedHashMap<>();
        
        res.put("ID", 60);
        res.put("Subject ID", 60);
        res.put("Path", 250);
        
        return res;
    }

    @Override
    public void addToDatabase() {
        try {
            Connection con = Method.getConnection();
            Statement s = con.createStatement();
            
            s.execute("insert into " + getClass().getSimpleName() + " (ID, subjectID, path, isLive)"
                      + " values ('"+getID()+"', '"+getSubjectID()+"', '"+getPath()+"', 'True')");
            
            con.close();
        } catch (SQLException e) {
            System.out.println("//  error in add database of exercise");
        }
    }

    @Override
    public void editDatabase() {
        try {
            Connection con = Method.getConnection();
            Statement s = con.createStatement();
            
            s.execute("update " + getClass().getSimpleName()
                      + " set subjectID='"+getSubjectID()+"', path='"+getPath()+"'"
                              + " where ID='" + getID() + "'");
            
            con.close();
        } catch (SQLException e) {
            System.out.println("//  error in edit database of exercise");
        }
    }
}