package resourceClass;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedHashMap;
import managedocumentoflecturer.Method;

/**
 *
 * @author Kieu Nhut Truong
 */
public class Score implements ObjectModel {

    private double ten;
    private double four;
    private String word;
    private boolean isValid;
    
    public Score(){
        ten = 0;
        four = 0;
        word = "";
    }
    
    public double getTen() {
        return ten;
    }

    public void setTen(double ten) {
        this.ten = ten;
    }

    public double getFour() {
        return four;
    }

    public void setFour(double four) {
        this.four = four;
    }

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    
    
    
    @Override
    public LinkedHashMap<String, Object> getProperties() {
        LinkedHashMap result = new LinkedHashMap();
        result.put("Ten", ten);
        result.put("Word", word);
        result.put("Four", four);
        return result;
    }

    @Override
    public void setPropertise(LinkedHashMap<String, Object> value) {
        value.forEach((t, u) -> {
            switch (t.trim().replaceAll(" ", "").toLowerCase()) {
                case "ten":
                    ten = Double.valueOf(u.toString().trim());
                    break;
                case "word":
                    word = u.toString();
                    break;
                case "four":
                    four = Double.valueOf(u.toString().trim());
                    break;
            }
        });
    }

    @Override
    public String[] getDeepKeys() {
        if(word.length()>0){
            return new String[]{"Ten"};
        }else{
            return new String[]{};
        }
    }

    @Override
    public boolean validateValue(LinkedHashMap<String, Object> value) {
        isValid = true;
        value.forEach((t, u) -> {
            if(ObjectModel.isNullString(u.toString())){
                isValid = false;
            }else switch (t.trim().replaceAll(" ", "").toLowerCase()) {
                case "ten":
                    try {
                        double te = Double.valueOf(u.toString().trim());
                        if (te<0 || te > 10) {
                            throw new Exception();
                        }
                    } catch (Exception e) {
                        isValid = false;
                    }
                    break;
                
                case "four":
                    try {
                        double te = Double.valueOf(u.toString().trim());
                        if (te<0||te>4) {
                            throw new Exception();
                        }
                    } catch (Exception e) {
                        isValid = false;
                    }
                    break;
            }
        });
        return isValid;
    }

    @Override
    public LinkedHashMap<String, Integer> getWidthCell() {
        LinkedHashMap result = new LinkedHashMap();
        result.put("Ten", 100);
        result.put("Word", 100);
        result.put("Four", 100);
        return result;
    }

    @Override
    public void addToDatabase() {
        if(!isValid){
            System.out.println(">> value is not available, so can not add to database.");
        }else try {
            Connection con = Method.getConnection();
            Statement s = con.createStatement();
            
            s.execute("insert into " + getClass().getSimpleName() + " (ten, word, four, isLive)"
                      + " values (" + ten + ", '" + word + "', " + four + ", 'True')");
            
            con.close();
        } catch (SQLException e) {
            System.out.println("//  error in add database of score");
        }
    }

    @Override
    public void editDatabase() {
        if(!isValid){
            System.out.println(">> value is not available, so can not edit to database.");
        }else try {
            Connection con = Method.getConnection();
            Statement s = con.createStatement();
            
            s.execute("update " + getClass().getSimpleName()
                      + " set word='" + word + "', four=" + four
                        + " where ten=" + ten);
            
            con.close();
        } catch (SQLException e) {
            System.out.println("//  error in add database of score");
        }
    }

    @Override
    public boolean canDelete() {
        return true;
    }

    @Override
    public void deleteDatabase() {
        try {
            Connection con = Method.getConnection();
            Statement s = con.createStatement();
            
            s.execute("delete from " + getClass().getSimpleName()
                      + " where ten=" + ten
            );
            
            con.close();
        } catch (SQLException e) {
            System.out.println("//  error in delete of score");
        }
    }
}