
package resourceClass;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedHashMap;
import managedocumentoflecturer.Method;


public class Thesis extends ProjectStudent{
    private String exID1, exID2, exID3;
    private double score1, score2, score3;
    
    public Thesis(){
        super();
        score1=0;
        score2=0;
        score3=0;
        exID1=":fk-Examiner:()";
        exID2=":fk-Examiner:()";
        exID3=":fk-Examiner:()";
    }
    
    @Override
    protected String createID() {
        String query = "select top 1 ID from "+getClass().getSimpleName()+" order by ID desc";
        short serial = 0;
        try {
            Connection con = Method.getConnection();
            Statement s = con.createStatement();
            ResultSet rs = s.executeQuery(query);
            if(rs.next()){
                String oldID = rs.getString("ID");
                serial = Short.valueOf(oldID.substring(oldID.lastIndexOf("-")+1).trim());
            }
        } catch (SQLException e) {
            System.out.println("// error in create ID");
        }
        
        return "th-" + (serial+1);
    }

    @Override
    public LinkedHashMap<String, Object> getProperties() {
        LinkedHashMap<String, Object> res = new LinkedHashMap<>();
        
        res.put("ID", getID());
        res.put("Name", getName());
        res.put("Semester", getSemester());
        res.put("School Year", getSchoolYear());
        res.put("Instructor", getInstructor());
        res.put("Topic ID", getTopicID());
        res.put("Subject ID", getSubjectID());
        res.put("Group ID", getGroupID());
        res.put("Score 1", getScore1());
        res.put("Score 2", getScore2());
        res.put("Score 3", getScore3());
        res.put("Ex ID 1", getExID1());
        res.put("Ex ID 2", getExID2());
        res.put("Ex ID 3", getExID3());
        res.put("Ten Score", getTenScore());
        res.put("Word Score", getWordScore());
        res.put("Path", getPath());

        return res;
    }

    @Override
    public String[] getDeepKeys() {
        return new String[]{"ID", "Ten Score","Word Score"};
    }

    @Override
    public void setPropertise(LinkedHashMap<String, Object> value) {
        value.forEach((k, v) -> {
            switch(k.trim().replaceAll(" ", "").toLowerCase()){
                case "id": setID(v.toString()); break;
                case "name":  setName(v.toString()); break;
                case "semester":  setSemester(Integer.valueOf(v.toString())); break;
                case "schoolyear":  setSchoolYear(v.toString()); break;
                case "instructor": setInstructor(v.toString()); break;
                case "topicid": setTopicID(v.toString()); break;
                case "subjectid": setSubjectID(v.toString()); break;
                case "groupid": setGroupID(v.toString()); break;
                case "path": setPath(v.toString()); break;
                case "score1": setScore1(Double.valueOf(v.toString())); break;
                case "score2": setScore2(Double.valueOf(v.toString())); break;
                case "score3": setScore3(Double.valueOf(v.toString())); break;
                case "exid1": setExID1(v.toString()); break;
                case "exid2": setExID2(v.toString()); break;
                case "exid3": setExID3(v.toString()); break;
            }
        });
        
        setTenScore((score1 + score2 + score3)/3);
        setWordScore(Method.switchScore(getTenScore()).get(0).toString());
    }

    @Override
    public boolean validateValue(LinkedHashMap<String, Object> value) {
        setIsValid(true);
        value.forEach((k, v) -> {
            if(ObjectModel.isNullString(v.toString())) setIsValid(false);
            else switch(k.trim().replaceAll(" ", "").toLowerCase()){
                case "semester":
                    try{
                        int val = Integer.valueOf(v.toString());
                        if( 1 > val || val > 3) throw new NumberFormatException();
                    }catch(NumberFormatException ex){
                        Method.Dialog(Method.DialogType.ERROR, "Error in input", ">> Semester is not available.");
                        setIsValid(false);
                    }
                    break;
                case "schoolyear":
                    if(!v.toString().matches("\\d+-\\d+")){
                        setIsValid(false);
                    }
                    break;
                case "score1":  case "score2":  case "score3":  
                    try{
                        double val = Double.valueOf(v.toString());
                        if(0 > val || val > 10){
                            throw new NumberFormatException();
                        }
                    }catch(NumberFormatException ex){
                        Method.Dialog(Method.DialogType.ERROR, "Error in input", ">> Score is not available.");
                        setIsValid(false);
                    }
                    break;
                    
                case "groupid":
                case "subjectid":
                case "exid1":case "exid2":case "exid3":
                case "instructor":
                case "topicid":
                    if(Method.parse2List(v.toString()).isEmpty()){
                        Method.Dialog(Method.DialogType.ERROR, "Error in input", ">> "+k+" is not available.");
                        setIsValid(false);
                    }
            }
        });
        return isValid();
    }

    @Override
    public LinkedHashMap<String, Integer> getWidthCell() {
        LinkedHashMap<String, Integer> res = new LinkedHashMap<>();
        
        res.put("ID", 60);
        res.put("Name", 220);
        res.put("Semester", 100);
        res.put("School Year", 180);
        res.put("Instructor", 200);
        res.put("Topic ID", 100);
        res.put("Subject ID", 100);
        res.put("Group ID", 100);
        res.put("Score 1", 80);
        res.put("Score 2", 80);
        res.put("Score 3", 80);
        res.put("Ex ID 1", 80);
        res.put("Ex ID 2", 80);
        res.put("Ex ID 3", 80);
        res.put("Ten Score", 100);
        res.put("Word Score", 100);
        res.put("Path", 100);
        
        return res;
    }

    

    public String getExID1() {
        return exID1;
    }

    public void setExID1(String exID1) {
        this.exID1 = exID1;
    }

    public String getExID2() {
        return exID2;
    }

    public void setExID2(String exID2) {
        this.exID2 = exID2;
    }

    public String getExID3() {
        return exID3;
    }

    public void setExID3(String exID3) {
        this.exID3 = exID3;
    }

    public double getScore1() {
        return score1;
    }

    public void setScore1(double score1) {
        this.score1 = score1;
    }

    public double getScore2() {
        return score2;
    }

    public void setScore2(double score2) {
        this.score2 = score2;
    }

    public double getScore3() {
        return score3;
    }

    public void setScore3(double score3) {
        this.score3 = score3;
    }
    
    @Override
    public void addToDatabase(){
        try {
            Connection con = Method.getConnection();
            Statement s = con.createStatement();
            
            System.out.println("insert into " + getClass().getSimpleName() + " (ID, name, semester, schoolYear, instructor, topicID, subjectID, tenScore, wordScore, path, groupID,isLive, exID1, exID2, exID3, score1, score2, score3)"
                      + " values ('"+getID()+"', '"+getName()+"', "+getSemester()+", '"+getSchoolYear()+"', '"+getInstructor()+"', '"+getTopicID()+"', '"+getSubjectID()+"', "+getTenScore()+", '"+getWordScore()+"', '"+getPath()+"', '"+getGroupID()+"', 'True',"
                              +"'"+ exID1 + "', '"+ exID2 + "', '"+ exID3 + "', "+ score1 +", "+ score2 +", "+ score3 +")");
            String qey="insert into " + getClass().getSimpleName() + " (ID, name, semester, schoolYear, instructor, topicID, subjectID, tenScore, wordScore, path, groupID,isLive, exID1, exID2, exID3, score1, score2, score3)"
                      + " values ('"+getID()+"', '"+getName()+"', "+getSemester()+", '"+getSchoolYear()+"', '"+getInstructor()+"', '"+getTopicID()+"', '"+getSubjectID()+"', "+getTenScore()+", '"+getWordScore()+"', '"+getPath()+"', '"+getGroupID()+"', 'True',"
                              +"'"+ exID1 + "', '"+ exID2 + "', '"+ exID3 + "', "+ score1 +", "+ score2 +", "+ score3 +")";
            s.execute(qey);
            
            con.close();
        } catch (SQLException e) {
            System.out.println("//  error in add database of thesis");
            e.printStackTrace();
        }
    }
    
    @Override
    public void editDatabase(){
        try {
            Connection con = Method.getConnection();
            Statement s = con.createStatement();
            
            s.execute("update " + getClass().getSimpleName()
                      + " set name='"+getName()+"', semester="+getSemester()+", schoolYear='"+getSchoolYear()+"', instructor='"+getInstructor()+"', "
                              + " topicID='"+getTopicID()+"', subjectID='"+getSubjectID()+"', tenScore="+getTenScore()+", wordScore='"+getWordScore()+"', path='"+getPath()+"'"
                                      + ", exID1='" + exID1 + "', exID2='" + exID2 + "', exID3='" + exID3 + "', score1=" + score1 +", score2=" + score2 + ", score3=" + score3
                      + " where ID='" + getID() + "'"
            );
            
            con.close();
        } catch (SQLException e) {
            System.out.println("//  error in delete of projectstudent");
        }
    }
}