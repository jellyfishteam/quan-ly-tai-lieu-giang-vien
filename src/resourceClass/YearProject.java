package resourceClass;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import managedocumentoflecturer.Method;

public class YearProject extends ProjectStudent{
    
    public YearProject(){
        super();
    }
    
    @Override
    protected String createID() {
        String query = "select top 1 ID from "+getClass().getSimpleName()+" order by ID desc";
        short serial = 0;
        try {
            Connection con = Method.getConnection();
            Statement s = con.createStatement();
            ResultSet rs = s.executeQuery(query);
            if(rs.next()){
                String oldID = rs.getString("ID");
                serial = Short.valueOf(oldID.substring(oldID.lastIndexOf("-")+1).trim());
            }
        } catch (SQLException e) {
            System.out.println("// error in create ID");
        }
        
        return "yp-" + (serial+1);
    }   
}
