package resourceClass;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedHashMap;
import managedocumentoflecturer.Method;

/**
 *
 * @author Kieu Nhut Truong
 */
public class StudentGroup implements ObjectModel {

    private String ID;
    private String members;
    private boolean isLive, valid;

    public StudentGroup(){
        ID = createID();
        members = ":fk-Student:[]";
        isLive = true;
    }
    
    private String createID() {
        String query = "select top 1 ID from "+getClass().getSimpleName()+" order by ID desc";
        short serial = 0;
        try {
            Connection con = Method.getConnection();
            Statement s = con.createStatement();
            ResultSet rs = s.executeQuery(query);
            if(rs.next()){
                String oldID = rs.getString("ID");
                serial = Short.valueOf(oldID.substring(oldID.lastIndexOf("-")+1).trim());
            }
        } catch (SQLException e) {
            System.out.println("// error in create ID");
        }
        
        return "g-" + (serial+1);
    }
    
    public void displayGroup() {
        getProperties().forEach((t, u) -> {
            System.out.println(t + " : " + u.toString());
        });
    }

    public boolean isValid() {
        return valid;
    }

    public void setValid(boolean valid) {
        this.valid = valid;
    }
    
    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getMembers() {
        return members;
    }

    public void setMembers(String members) {
        this.members = members;
    }

    @Override
    public LinkedHashMap<String, Object> getProperties() {
        LinkedHashMap<String, Object> res = new LinkedHashMap<>();

        res.put("ID", ID);
        res.put("Members", members);

        return res;
    }

    @Override
    public void setPropertise(LinkedHashMap<String, Object> value) {
        value.forEach((k, v) -> {
            switch (k.toLowerCase().trim().replaceAll(" ", "")) {
                case "id":
                    ID = v.toString();
                    break;
                case "members":
                    members = v.toString();
                    break;
            }
        });
    }

    @Override
    public boolean validateValue(LinkedHashMap<String, Object> value) {
        setValid(true);
        value.forEach((k, v) -> {
            if(ObjectModel.isNullString(v.toString())) setValid(false);
            else switch(k.trim().replaceAll(" ", "").toLowerCase()){
                case "members":
                    if(Method.parse2List(v.toString()).isEmpty()){
                        Method.Dialog(Method.DialogType.ERROR, "Error in input", ">> "+k+" is not available.");
                        setValid(false);
                    }
            }
        });
        return valid;
    }
    
    @Override
    public String[] getDeepKeys() {
        return new String[]{"ID"};
    }

    @Override
    public LinkedHashMap<String, Integer> getWidthCell() {
        LinkedHashMap<String, Integer> res = new LinkedHashMap<>();

        res.put("ID", 100);
        res.put("Members", 200);

        return res;
    }
    
    @Override
    public void addToDatabase() {
        try {
            Connection con = Method.getConnection();
            Statement s = con.createStatement();
            
            s.execute("insert into " + getClass().getSimpleName() + " (ID, members, isLive)"
                      + " values ('"+ID+"', '"+members+"', 'True')");
            
            con.close();
        } catch (SQLException e) {
            System.out.println("//  error in add database of projectstudent");
        }
    }

    @Override
    public void editDatabase() {
        try {
            Connection con = Method.getConnection();
            Statement s = con.createStatement();
            
            s.execute("update " + getClass().getSimpleName()
                      + " set members='"+members+"'"
                              + " where ID='" + ID+ "'");
            
            con.close();
        } catch (SQLException e) {
            System.out.println("//  error in add database of projectstudent");
        }
    }
    
    
    @Override
    public boolean canDelete(){
        String res = "";
        try {
            Connection con = Method.getConnection();
            Statement s = con.createStatement();
            
            ResultSet rs = s.executeQuery("select * from Essay where groupID like '%" + ID + "%'");
            while(rs.next()){
                res += rs.getString("ID") + ", " ;
            }
            
            rs = s.executeQuery("select * from OtherProject where groupID like '%" + ID + "%'");
            while(rs.next()){
                res += rs.getString("ID") + ", " ;
            }
            
            rs = s.executeQuery("select * from YearProject where groupID like '%" + ID + "%'");
            while(rs.next()){
                res += rs.getString("ID") + ", " ;
            }
            
            rs = s.executeQuery("select * from Thesis where groupID like '%" + ID + "%'");
            while(rs.next()){
                res += rs.getString("ID") + ", " ;
            }
            
            if(res.length()>0){
                System.out.println("Can not delete, because group " + ID + " is the group of project: " + Method.delete2space(res));
            }
        } catch (SQLException e) {
            System.out.println("// error in class student, can delete...");
        }
        return res.length()==0;
    }
    
    @Override
    public void deleteDatabase() {
        try {
            Connection con = Method.getConnection();
            Statement s = con.createStatement();
            
            s.execute("delete from " + getClass().getSimpleName()
                      + " where ID='" + getID() + "'"
            );
            
            con.close();
        } catch (SQLException e) {
            System.out.println("//  error in delete of student group");
        }
    }
}