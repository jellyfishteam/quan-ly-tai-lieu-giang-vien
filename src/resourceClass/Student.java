
package resourceClass;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.LinkedHashMap;
import managedocumentoflecturer.Method;

/**
 *
 * @author nmhillusion
 */
public class Student implements ObjectModel{
    private String ID;
    private String fullName;
    private String major;
    private String college;
    private String email;
    private LocalDate dateOfBirth;
    private String Class;
    private int course;
    private boolean isValid, isMale;

    public Student(){
        ID = "";
        fullName = "";
        major = "";
        college = "";
        isMale = true;
        course = 40;
        dateOfBirth = LocalDate.now();
        Class = "";
        email = "@email";
    }
    
    public Student(String ID, String fullName, String major, String college, int course, boolean isMale, LocalDate dateOfBirth, String Class) {
        this.ID = ID;
        this.fullName = fullName;
        this.major = major;
        this.college = college;
        if(fullName.lastIndexOf(" ")!=-1){
            email = fullName.substring(fullName.lastIndexOf(" ")+1) + ID + "@student.ctu.edu.vn";
        }else{
            email = fullName + ID + "@student.ctu.edu.vn";
        }
        this.course = course;
        this.isMale = isMale;
        this.dateOfBirth = dateOfBirth;
        this.Class = Class;
    }
    
    /**
     * Hiển thị thông tin về đối tượng (dùng cho việc kiểm tra)
     */
    public void displayStudent(){
        getProperties().forEach((t, u) -> {
            System.out.println(t + " : " + u.toString());
        });
    }

    public String getID() {
        return ID;
    }

    public String getFullName() {
        return fullName;
    }

    public String getMajor() {
        return major;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = LocalDate.parse(dateOfBirth);
    }

    public void setClass(String Class) {
        this.Class = Class;
    }

    public void setIsMale(boolean isMale) {
        this.isMale = isMale;
    }

    public LocalDate getDateOfBirth() {
        return dateOfBirth;
    }

    public String getiClass() {
        return Class;
    }

    public boolean isIsMale() {
        return isMale;
    }

    public String getCollege() {
        return college;
    }

    public String getEmail() {
        return email;
    }

    public int getCourse() {
        return course;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public void setMajor(String major) {
        this.major = major;
    }

    public void setCollege(String college) {
        this.college = college;
    }

    public void setCourse(short course) {
        this.course = course;
    }

    @Override
    public LinkedHashMap<String, Object> getProperties() {
        LinkedHashMap<String, Object> res = new LinkedHashMap<>();
        
        res.put("ID", ID);
        res.put("Full Name", fullName);
        res.put("is Male", isMale);
        res.put("Date of Birth", dateOfBirth);
        res.put("Major", major);
        res.put("Class", Class);
        res.put("College", college);
        res.put("Email", email);
        res.put("Course", course);
        
        return res;
    }

    @Override
    public void setPropertise(LinkedHashMap<String, Object> value) {
        value.forEach((k, v) -> {
            switch(k.trim().replaceAll(" ", "").toLowerCase()){
                case "id":  ID = v.toString().trim(); break;
                case "fullname":  fullName = v.toString().trim(); break;
                case "major":  major = v.toString(); break;
                case "college":  college = v.toString(); break;
                case "course":  course = Integer.valueOf(v.toString()); break;
                case "ismale": isMale = Boolean.valueOf(v.toString()); break;
                case "dateofbirth": dateOfBirth = LocalDate.parse(v.toString()); break;
                case "class": Class = v.toString(); break;
            }
        });
        
        if(fullName.lastIndexOf(" ")!=-1){
            email = fullName.substring(fullName.lastIndexOf(" ")+1) + ID + "@student.ctu.edu.vn";
        }else{
            email = fullName + ID + "@student.ctu.edu.vn";
        }
    }

    @Override
    public boolean validateValue(LinkedHashMap<String, Object> value) {
        isValid = true;
        value.forEach((k, v) -> {
            if(ObjectModel.isNullString(v.toString())) isValid = false;
            else switch(k.trim().toLowerCase().replaceAll(" ", "")){
                case "course":  
                    try{
                        if(Integer.valueOf(v.toString()) <= 0){
                            throw new NumberFormatException();
                        }
                    }catch(NumberFormatException ex){
                        System.out.println(">> Course is not available.");
                        isValid = false;
                    }
                    break;
            }
        });
        
        return isValid;
    }

    @Override
    public String[] getDeepKeys() {
        if(ID.length()<1){
            return new String[]{"Email"};
        }else{
            return new String[]{"ID", "Email"};
        }
    }

    @Override
    public LinkedHashMap<String, Integer> getWidthCell() {
        LinkedHashMap<String, Integer> data = new LinkedHashMap<>();
        
        data.put("ID", 90);
        data.put("Full Name", 200);
        data.put("isMale", 100);
        data.put("Date of Birth", 100);
        data.put("Major", 160);
        data.put("Class", 100);
        data.put("College", 160);
        data.put("Email", 260);
        data.put("Course", 100);        
                
        return data;
    }
    
    @Override
    public void addToDatabase() {
        try {
            Connection con = Method.getConnection();
            Statement s = con.createStatement();
            
            System.out.println("insert into " + getClass().getSimpleName() + " (ID, Class, college, course, dateOfBirth, email, fullName, isMale, major, isLive)"
                      + " values ('"+getID()+"', '"+getiClass()+"', '"+getCollege()+"', "+course+", '"+dateOfBirth+"', '"+email+"', '"+fullName+"', '"+isMale+"', '"+major+"','True')");
            
            s.execute("insert into " + getClass().getSimpleName() + " (ID, Class, college, course, dateOfBirth, email, fullName, isMale, major, isLive)"
                      + " values ('"+getID()+"', '"+getiClass()+"', '"+getCollege()+"', "+course+", '"+dateOfBirth+"', '"+email+"', '"+fullName+"', '"+isMale+"', '"+major+"','True')");
            
            con.close();
        } catch (SQLException e) {
            System.out.println("//  error in add database of student");
        }
    }

    @Override
    public void editDatabase() {
        try {
            Connection con = Method.getConnection();
            Statement s = con.createStatement();
            
            s.execute("update " + getClass().getSimpleName()
                      + " set ID='"+getID()+"', Class='"+getiClass()+"', college='"+getCollege()+"', course="+course+", dateOfBirth='"+dateOfBirth+"', email='"+email+"', fullName='"+fullName+"', isMale='"+isMale+"'"
                              + " where ID='" + ID + "'");
            
            con.close();
        } catch (SQLException e) {
            System.out.println("//  error in add database of projectstudent");
        }
    }

    @Override
    public void deleteDatabase() {
        try {
            Connection con = Method.getConnection();
            Statement s = con.createStatement();
            
            s.execute("delete from " + getClass().getSimpleName()
                      + " where ID='" + getID() + "'"
            );
            
            con.close();
        } catch (SQLException e) {
            System.out.println("//  error in delete of student");
        }
    }
    
    @Override
    public boolean canDelete(){
        String res = "";
        try {
            Connection con = Method.getConnection();
            Statement s = con.createStatement();
            
            ResultSet rs = s.executeQuery("select * from StudentGroup where members like '%" + ID + "%' and isLive='True'");
            while(rs.next()){
                res += rs.getString("ID") + ", ";
            }
            if(res.length()>0){
                System.out.println("Can not delete, because " + fullName + " is a member of group: " + Method.delete2space(res));
            }
        } catch (SQLException e) {
            System.out.println("// error in class student, can delete...");
        }
        return res.length()==0;
    }
}