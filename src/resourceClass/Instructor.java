package resourceClass;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedHashMap;
import managedocumentoflecturer.Method;

/**
 *
 * @author Kieu Nhut Truong
 */
public class Instructor implements ObjectModel {

    private String ID;
    private String fullName;
    private String occupation;
    private String phoneNumber;
    private String address;
    private boolean isValid, isLive;
    

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getMajor() {
        return occupation;
    }

    public void setMajor(String occupation) {
        this.occupation = occupation;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
    
    public Instructor(){
        ID = createID();
        fullName = "";
        occupation = "";
        phoneNumber = "";
        address = "";
        isLive = false;
    }
    
    private String createID() {
        String query = "select top 1 ID from "+getClass().getSimpleName()+" order by ID desc";
        short serial = 0;
        try {
            Connection con = Method.getConnection();
            Statement s = con.createStatement();
            ResultSet rs = s.executeQuery(query);
            if(rs.next()){
                String oldID = rs.getString("ID");
                serial = Short.valueOf(oldID.substring(oldID.lastIndexOf("-")+1).trim());
            }
        } catch (SQLException e) {
            System.out.println("// error in create ID");
        }
        
        return "itr-" + (serial+1);
    }
    
    @Override
    public LinkedHashMap<String, Object> getProperties() {
        LinkedHashMap<String, Object> res = new LinkedHashMap<>();

        res.put("ID", ID);
        res.put("Full Name", fullName);
        res.put("Occupation", occupation);
        res.put("Phone Number", phoneNumber);
        res.put("Address", address);
        res.put("isLive" , isLive);
        
        return res;
    }

    @Override
    public void setPropertise(LinkedHashMap<String, Object> value) {
        value.forEach((k, v) -> {
            switch (k.trim().replaceAll(" ", "").toLowerCase()) {
                case "id":
                    ID = v.toString();
                    break;
                case "fullname":
                    fullName = v.toString();
                    break;
                case "occupation":
                    occupation = v.toString();
                    break;
                case "phonenumber":
                    phoneNumber = v.toString();
                    break;
                case "address":
                    address = v.toString();
                    break;
                }
            
        });
    }

    @Override
    public boolean validateValue(LinkedHashMap<String, Object> value) {
        isValid = true;
        value.forEach((k, v) -> {
            if (ObjectModel.isNullString(v.toString())) {
                isValid = false;
            } else {
                switch (k.trim().replaceAll(" ", "").toLowerCase()) {
                    case "phonenumber":
                        try {
                            Integer.valueOf(v.toString());
                        } catch (NumberFormatException ex) {
                            Method.Dialog(Method.DialogType.ERROR, "Error in input", ">> "+k+" is not available.");
                            isValid = false;
                        }
                        break;
                }

            }
        });

        return isValid;
    }

    @Override
    public String[] getDeepKeys() {
        return new String[]{"ID"};
    }

    @Override
    public LinkedHashMap<String, Integer> getWidthCell() {
        LinkedHashMap<String, Integer> data = new LinkedHashMap<>();
        
        data.put("ID", 90);
        data.put("Full Name", 200);
        data.put("Occupation", 160);
        data.put("Phone Number", 100);
        data.put("Address", 260);

        return data;
    }
    
    @Override
    public void addToDatabase() {
        try {
            Connection con = Method.getConnection();
            Statement s = con.createStatement();
            
            s.execute("insert into " + getClass().getSimpleName() + " (ID, fullName, occupation, phoneNumber, address,isLive)"
                      + " values ('"+getID()+"', '"+getFullName()+"', "+occupation+", '"+getPhoneNumber()+"', '"+address+"', 'True')");
            
            con.close();
        } catch (SQLException e) {
            System.out.println("//  error in add database of instructor");
        }
    }

    @Override
    public void editDatabase() {
        try {
            Connection con = Method.getConnection();
            Statement s = con.createStatement();
            
            s.execute("update " + getClass().getSimpleName()
                      + " set fullName='"+getFullName()+"', occupation='"+occupation+"', phoneNumber='"+getPhoneNumber()+"', address='"+address+"'"
                              + " where ID='" + getID() + "'");
            
            con.close();
        } catch (SQLException e) {
            System.out.println("//  error in edit database of instructor");
        }
    }
    
    @Override
    public boolean canDelete(){
        String res = "";
        try {
            Connection con = Method.getConnection();
            Statement s = con.createStatement();
            
            ResultSet rs = s.executeQuery("select * from Essay where instructor like '%" + ID + "%'");
            while(rs.next()){
                res += rs.getString("ID") + ", " ;
            }
            
            rs = s.executeQuery("select * from OtherProject where instructor like '%" + ID + "%'");
            while(rs.next()){
                res += rs.getString("ID") + ", " ;
            }
            
            rs = s.executeQuery("select * from YearProject where instructor like '%" + ID + "%'");
            while(rs.next()){
                res += rs.getString("ID") + ", " ;
            }
            
            rs = s.executeQuery("select * from Thesis where instructor like '%" + ID + "%'");
            while(rs.next()){
                res += rs.getString("ID") + ", " ;
            }
            
            if(res.length()>0){
                Method.Dialog(Method.DialogType.ERROR, "Can not Delete!!!","Can not delete, because " + fullName + " is a instructor of project(s): " + Method.delete2space(res));
            }
        } catch (SQLException e) {
            System.out.println("// error in class instructor, can delete...");
        }
        return res.length()==0;
    }

    @Override
    public void deleteDatabase() {
        try {
            Connection con = Method.getConnection();
            Statement s = con.createStatement();
            
            s.execute("delete from " + getClass().getSimpleName()
                      + " where ID='" + getID() + "'"
            );
            
            con.close();
        } catch (SQLException e) {
            System.out.println("//  error in delete of instructor");
        }
    }
}
