
package resourceClass;

import java.io.File;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import managedocumentoflecturer.Method;


abstract class TeachingMaterial implements ObjectModel{

    private String ID;
    private String subjectID;
    private String path;
    private boolean isLive;
    private boolean isValid;
    
    public TeachingMaterial(){
        this.ID= createID();
        this.subjectID=":fk-Subject:()";
        this.path="";
    }
    
    abstract protected String createID();
    
    public TeachingMaterial(String ID, String subjectID, String path){
        this.ID=ID;
        this.subjectID=subjectID;
        this.path=path;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getSubjectID() {
        return subjectID;
    }

    public void setSubjectID(String subjectID) {
        this.subjectID = subjectID;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        if(this.path.length() > 1){
            File f = new File(this.path);
            if(!f.delete()){
                f.deleteOnExit();
            }
        }
        
        this.path = Method.moveFile(path, "DataUser//TeachingMaterial//" + getClass().getSimpleName());
    }

    public boolean isLive() {
        return isLive;
    }

    public void setIsLive(boolean isLive) {
        this.isLive = isLive;
    }

    public boolean isValid() {
        return isValid;
    }

    public void setIsValid(boolean isValid) {
        this.isValid = isValid;
    }
    
    @Override
    public boolean canDelete(){
        return true;
    }
    
    @Override
    public void deleteDatabase() {
        try {
            Connection con = Method.getConnection();
            Statement s = con.createStatement();
            
            s.execute("delete from " + getClass().getSimpleName()
                      + " where ID='" + getID() + "'"
            );
            
            con.close();
        } catch (SQLException e) {
            System.out.println("//  error in delete of teachingMaterial");
        }
    }
}
