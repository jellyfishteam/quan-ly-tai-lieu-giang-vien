
package resourceClass;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedHashMap;
import managedocumentoflecturer.Method;

public class Exam extends TeachingMaterial{
    private String schoolYear;
    private int semester;
    
    public Exam(){
        super();
        schoolYear = "0-1";
        semester = 1;
    }
    
    @Override
    protected String createID() {
        String query = "select top 1 ID from "+getClass().getSimpleName()+" order by ID desc";
        short serial = 0;
        try {
            Connection con = Method.getConnection();
            Statement s = con.createStatement();
            ResultSet rs = s.executeQuery(query);
            if(rs.next()){
                String oldID = rs.getString("ID");
                serial = Short.valueOf(oldID.substring(oldID.lastIndexOf("-")+1).trim());
            }
        } catch (SQLException e) {
            System.out.println("// error in create ID");
        }
        
        return "Exam-" + (serial+1);
    }

    
    @Override
    public LinkedHashMap<String, Object> getProperties() {
        LinkedHashMap<String, Object> res = new LinkedHashMap<>();
        
        res.put("ID", getID());
        res.put("Subject ID", getSubjectID());
        res.put("Semester", getSemester());
        res.put("School Year", getSchoolYear());
        res.put("Path", getPath());
        
        return res;
    }

    @Override
    public String[] getDeepKeys() {
        return new String[]{"ID"};
    }

    @Override
    public void setPropertise(LinkedHashMap<String, Object> value) {
        value.forEach((String k, Object v) -> {
            switch (k.trim().replaceAll(" ", "").toLowerCase()) {
                case "id":
                    setID(v.toString());
                    break;
                case "subjectid":
                    setSubjectID(v.toString());
                    break;
                case "semester":
                    setSemester(Integer.valueOf(v.toString()));
                    break;
                case "schoolyear":
                    setSchoolYear(v.toString());
                    break;
                case "path":
                    setPath(v.toString());
                    break;
            }
        });
    }

    @Override
    public boolean validateValue(LinkedHashMap<String, Object> value) {
        setIsValid(true);
        value.forEach((k, v) -> {
            if(!ObjectModel.isNullString(v.toString())) setIsValid(false);
            else switch(k.trim().replaceAll(" ", "").toLowerCase()){
                case "semester":
                    try{
                        int val = Integer.valueOf(v.toString());
                        if( 1 > val || val > 3) throw new NumberFormatException();
                    }catch(NumberFormatException ex){
                        Method.Dialog(Method.DialogType.ERROR, "Error in input", ">> Semester is not available.");
                        setIsValid(false);
                    }
                    break;
                case "schoolyear":
                    if(!v.toString().matches("\\d+-\\d+")){
                        setIsValid(false);
                    }
                    break;
                    
                case "subjectid":
                    if(Method.parse2List(v.toString()).isEmpty()){
                        Method.Dialog(Method.DialogType.ERROR, "Error in input", ">> "+k+" is not available.");
                        setIsValid(false);
                    }
                
            }
        });
        return isValid();
    }

    @Override
    public LinkedHashMap<String, Integer> getWidthCell() {
         LinkedHashMap<String, Integer> res = new LinkedHashMap<>();
        
        res.put("ID", 60);
        res.put("Subject ID", 60);
        res.put("Semester", 20);
        res.put("School Year", 180);
        res.put("Path", 250);
        
        return res;
    }

    public String getSchoolYear() {
        return schoolYear;
    }

    public void setSchoolYear(String schoolYear) {
        this.schoolYear = schoolYear;
    }

    public int getSemester() {
        return semester;
    }

    public void setSemester(int semester) {
        this.semester = semester;
    }

    @Override
    public void addToDatabase() {
        try {
            Connection con = Method.getConnection();
            Statement s = con.createStatement();
            
            s.execute("insert into " + getClass().getSimpleName() + " (ID, subjectID, semester, schoolYear, path, isLive)"
                      + " values ('"+getID()+"', '"+getSubjectID()+"', "+getSemester()+", '"+getSchoolYear()+"', '"+getPath()+"', 'True')");
            
            con.close();
        } catch (SQLException e) {
            System.out.println("//  error in add database of Exam");
        }
    }

    @Override
    public void editDatabase() {
        try {
            Connection con = Method.getConnection();
            Statement s = con.createStatement();
            
            s.execute("update " + getClass().getSimpleName()
                      + " set subjectID='"+getSubjectID()+"', semester="+getSemester()+", schoolYear='"+getSchoolYear()+"', path='"+getPath()+"'"
                              + " where ID='" + getID() + "'");
            
            con.close();
        } catch (SQLException e) {
            System.out.println("//  error in edit database of Exam");
        }
    }
}