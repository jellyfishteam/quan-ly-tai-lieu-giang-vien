
package resourceClass;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedHashMap;
import managedocumentoflecturer.Method;


public class Topic implements ObjectModel{
    private String ID, name;
    private boolean isLive;
    
    public Topic(){
        ID=createID();
        name="";
        isLive=false;
    }
    
    private String createID() {
        String query = "select top 1 ID from "+getClass().getSimpleName()+" order by ID desc";
        short serial = 0;
        try {
            Connection con = Method.getConnection();
            Statement s = con.createStatement();
            ResultSet rs = s.executeQuery(query);
            if(rs.next()){
                String oldID = rs.getString("ID");
                serial = Short.valueOf(oldID.substring(oldID.lastIndexOf("-")+1).trim());
            }
        } catch (SQLException e) {
            System.out.println("// error in create ID");
        }
        
        return "to-" + (serial+1);
    }
    
    public Topic(String ID, String name, boolean isLive){
        this.ID=ID;
        this.name=name;
        this.isLive=isLive;
    }
    @Override
    public LinkedHashMap<String, Object> getProperties() {
         LinkedHashMap<String, Object> res = new LinkedHashMap<>();

        res.put("ID", ID);
        res.put("Name", name);

        return res;
    }

    @Override
    public String[] getDeepKeys() {
        return new String[]{"ID"};
    }

    @Override
    public void setPropertise(LinkedHashMap<String, Object> value) {
        value.forEach((k, v) -> {
            switch (k.trim().replaceAll(" ", "").toLowerCase()) {
                case "id":
                    ID = v.toString();
                    break;
                case "name":
                    name = v.toString();
                    break;
            }
        });
    }

    

    @Override
    public boolean validateValue(LinkedHashMap<String, Object> value) {
        return !ObjectModel.isNullString(value.get("Name").toString());
    }

    @Override
    public LinkedHashMap<String, Integer> getWidthCell() {
        LinkedHashMap<String, Integer> res = new LinkedHashMap<>();

        res.put("ID", 60);
        res.put("Name", 210);

        return res;
    }


    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isIsLive() {
        return isLive;
    }

    public void setIsLive(boolean isLive) {
        this.isLive = isLive;
    }
    
    @Override
    public void addToDatabase() {
        try {
            Connection con = Method.getConnection();
            Statement s = con.createStatement();
            
            s.execute("insert into " + getClass().getSimpleName() + " (ID, name, isLive)"
                      + " values ('"+getID()+"', '"+getName()+"', 'True')");
            
            con.close();
        } catch (SQLException e) {
            System.out.println("//  error in add database of projectstudent");
        }
    }

    @Override
    public void editDatabase() {
        try {
            Connection con = Method.getConnection();
            Statement s = con.createStatement();
            
            s.execute("update " + getClass().getSimpleName()
                      + " set name='"+name+"'"
                              + " where ID='" + getID() + "'");
            
            con.close();
        } catch (SQLException e) {
            System.out.println("//  error in add database of projectstudent");
        }
    }
    
    
    @Override
    public boolean canDelete(){
        String res = "";
        try {
            Connection con = Method.getConnection();
            Statement s = con.createStatement();
            
            ResultSet rs = s.executeQuery("select * from Essay where topicID like '%" + ID + "%'");
            while(rs.next()){
                res += rs.getString("ID") + ", " ;
            }
            
            rs = s.executeQuery("select * from OtherProject where topicID like '%" + ID + "%'");
            while(rs.next()){
                res += rs.getString("ID") + ", " ;
            }
            
            rs = s.executeQuery("select * from YearProject where topicID like '%" + ID + "%'");
            while(rs.next()){
                res += rs.getString("ID") + ", " ;
            }
            
            rs = s.executeQuery("select * from Thesis where topicID like '%" + ID + "%'");
            while(rs.next()){
                res += rs.getString("ID") + ", " ;
            }
            
            if(res.length()>0){
                Method.Dialog(Method.DialogType.ERROR, "Error in input", "Can not delete, because " + name + " is a topic of project(s): " + Method.delete2space(res));
            }
        } catch (SQLException e) {
            System.out.println("// error in class topic, can delete...");
        }
        return res.length()==0;
    }

    @Override
    public void deleteDatabase() {
        try {
            Connection con = Method.getConnection();
            Statement s = con.createStatement();
            
            s.execute("delete from " + getClass().getSimpleName()
                      + " where ID='" + getID() + "'"
            );
            
            con.close();
        } catch (SQLException e) {
            System.out.println("//  error in delete of Topic");
        }
    }
}