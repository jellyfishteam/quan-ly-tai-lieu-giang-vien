package resourceClass;

import java.util.LinkedHashMap;

/**
 *
 * @author nmhillusion
 */
public interface ObjectModel {
    
    /**
     * Lấy về danh sách từng cặp <b>thuộc tính - giá trị</b> của đối tượng/lớp
     * @return danh sách từng cặp <b>thuộc tính - giá trị</b> của đối tượng/lớp
     */
    public LinkedHashMap<String, Object> getProperties();
    
    /**
     * Lấy về các trường không cho người dùng chỉnh sửa thông tin
     * @return mảng tên các trường không cho người dùng chỉnh sửa thông tin
     */
    public String[] getDeepKeys();
    
    /**
     * Cài đặt thuộc tính cho đối tượng này từ danh sách từng cặp <b>thuộc tính - giá trị</b>
     * @param value danh sách từng cặp <b>thuộc tính - giá trị</b> bạn muốn thiết lập
     */
    public void setPropertise(LinkedHashMap<String, Object> value);
    
    /**
     * Kiểm tra xem chuỗi nhập vào có rỗng không
     * @param val chuỗi cần kiểm tra
     * @return chuỗi có rỗng không
     */
    public static boolean isNullString(String val){
        if(val.trim().compareTo("")==0){
            System.out.println(" Value must be not null.");
            return true;
        }else{
            return false;
        }
    }
    
    /**
     * Kiểm tra tính hợp lệ của các dữ liệu trước khi cập nhật cho đối tượng
     * @param value có hợp lệ hay không
     * @return value is available
     */
    public boolean validateValue(LinkedHashMap<String, Object> value);
    
    /**
     * Lấy độ dài của từng trường dữ liệu trong đối tượng
     * @return danh sách các trường dữ liệu và độ dài cell được đặt
     */
    public LinkedHashMap<String, Integer> getWidthCell();
    
    /**
     * Thêm mới record này vào cơ sở dữ liệu
     */
    public void addToDatabase();
    
    /**
     * Cập nhật lại giá trị vào cơ sở dữ liệu
     */
    public void editDatabase();
    
    /**
     * Kiểm tra xem phần tử muốn xóa có là khóa ngoại của lớp nào không
     * @return kết quả kiểm tra
     */
    public boolean canDelete();
    
    /**
     * Xóa record này khỏi cơ sở dữ liệu
     */
    public void deleteDatabase();
}