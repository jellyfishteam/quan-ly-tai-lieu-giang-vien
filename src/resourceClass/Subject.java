package resourceClass;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedHashMap;
import managedocumentoflecturer.Method;

public class Subject implements ObjectModel{
    private String ID, subjectName;
    private boolean isLive ;

    public Subject(){
        ID="";
        subjectName="";
        isLive=false;
    }
    
    public Subject(String ID, String subjectName, boolean isLive){
        this.ID=ID;
        this.subjectName=subjectName;
        this.isLive=isLive;
    }
    
    @Override
    public LinkedHashMap<String, Object> getProperties() {
        LinkedHashMap<String, Object> res = new LinkedHashMap<>();

        res.put("ID", ID);
        res.put("Subject Name", subjectName);

        return res;
    }

    @Override
    public String[] getDeepKeys() {
        if(ID.length()<1){
            return new String[]{};
        }else{
            return new String[]{"ID"};
        }
    }

    @Override
    public void setPropertise(LinkedHashMap<String, Object> value) {
        value.forEach((k, v) -> {
            switch (k.trim().replaceAll(" ", "").toLowerCase()) {
                case "id":
                    ID = v.toString();
                    break;
                case "subjectname":
                    subjectName = v.toString();
                    break;
            }
        });
    }

    @Override
    public boolean validateValue(LinkedHashMap<String, Object> value) {
        return !ObjectModel.isNullString(value.get("Subject Name").toString());
    }

    @Override
    public LinkedHashMap<String, Integer> getWidthCell() {
        LinkedHashMap<String, Integer> res = new LinkedHashMap<>();

        res.put("ID", 60);
        res.put("Subject Name", 210);

        return res;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getSubjectName() {
        return subjectName;
    }

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

    public boolean isIsLive() {
        return isLive;
    }

    public void setIsLive(boolean isLive) {
        this.isLive = isLive;
    }   
    
    @Override
    public void addToDatabase() {
        try {
            Connection con = Method.getConnection();
            Statement s = con.createStatement();
            
            s.execute("insert into " + getClass().getSimpleName() + " (ID, subjectName, isLive)"
                      + " values ('"+getID()+"', '"+getSubjectName()+"', 'True')");
            
            con.close();
        } catch (SQLException e) {
            System.out.println("//  error in add database of subject");
        }
    }

    @Override
    public void editDatabase() {
        try {
            Connection con = Method.getConnection();
            Statement s = con.createStatement();
            
            s.execute("update " + getClass().getSimpleName()
                      + " set subjectName='"+subjectName+"'"
                              + " where ID='" + getID() + "'");
            
            con.close();
        } catch (SQLException e) {
            System.out.println("//  error in edit database of subject");
        }
    }

    
    @Override
    public boolean canDelete(){
        String res = "";
        try {
            Connection con = Method.getConnection();
            Statement s = con.createStatement();
            
            ResultSet rs = s.executeQuery("select * from Essay where subjectID like '%" + ID + "%'");
            while(rs.next()){
                res += rs.getString("ID") + ", " ;
            }
            
            rs = s.executeQuery("select * from OtherProject where subjectID like '%" + ID + "%'");
            while(rs.next()){
                res += rs.getString("ID") + ", " ;
            }
            
            rs = s.executeQuery("select * from YearProject where subjectID like '%" + ID + "%'");
            while(rs.next()){
                res += rs.getString("ID") + ", " ;
            }
            
            rs = s.executeQuery("select * from Thesis where subjectID like '%" + ID + "%'");
            while(rs.next()){
                res += rs.getString("ID") + ", " ;
            }
            
            rs = s.executeQuery("select * from Exam where subjectID like '%" + ID + "%'");
            while(rs.next()){
                res += rs.getString("ID") + ", " ;
            }
            
            rs = s.executeQuery("select * from LessonPlan where subjectID like '%" + ID + "%'");
            while(rs.next()){
                res += rs.getString("ID") + ", " ;
            }
            
            rs = s.executeQuery("select * from Exercise where subjectID like '%" + ID + "%'");
            while(rs.next()){
                res += rs.getString("ID") + ", " ;
            }
            
            if(res.length()>0){
                Method.Dialog(Method.DialogType.ERROR, "Error in input", "Can not delete, because " + subjectName + " is a subject of: " + Method.delete2space(res));
            }
        } catch (SQLException e) {
            System.out.println("// error in class topic, can delete...");
        }
        return res.length()==0;
    }
    
    @Override
    public void deleteDatabase() {
        try {
            Connection con = Method.getConnection();
            Statement s = con.createStatement();
            
            s.execute("delete from " + getClass().getSimpleName()
                      + " where ID='" + getID() + "'"
            );
            
            con.close();
        } catch (SQLException e) {
            System.out.println("//  error in delete of subject");
        }
    }
}