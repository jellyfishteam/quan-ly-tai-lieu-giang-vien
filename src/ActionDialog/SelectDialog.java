package ActionDialog;

import GUI.ClassTab;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.stage.StageStyle;

/**
 *
 * @author nmhillusion
 */
public class SelectDialog extends ModelForm{
    private String searchClass;
    private boolean selectMulty = false;
    
    public SelectDialog(String searchCls, boolean isMulty){
        super("Search in " + searchCls, new String[]{});
        searchClass = searchCls;
        selectMulty = isMulty;
        setConent();
    }
    
    @Override
    protected void setContent(String[] key) {
        
    }
    
    private void setConent(){
        ClassTab content = new ClassTab(searchClass).setMultySelect(selectMulty);
        content.setContent("select");
        add(content.getContent());
        setStageStyle(StageStyle.UTILITY);
        
        ButtonType btnSelect = new ButtonType("Select", ButtonBar.ButtonData.OK_DONE);
        addButtonAction(btnSelect, ButtonType.CANCEL);
        
        setResult((btnClicked) -> {
            if(btnClicked == btnSelect){
                return content.getSelectedItems();
            }
            return null;
        });
    }
    
    /**
     * Cài đặt có cho người dùng chọn đồng thời nhiều item không
     * @param val có cho người dùng chọn đồng thời nhiều item không
     */
    public void setSelectMulty(boolean val){
        selectMulty = val;
    }
}
