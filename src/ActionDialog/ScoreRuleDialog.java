package ActionDialog;

import GUI.ClassTab;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.stage.StageStyle;
import resourceClass.ObjectModel;

/**
 * created in 26/04.2017
 * @author nmhillusion
 */
public class ScoreRuleDialog extends ModelForm{
    private ObjectModel object;
    
    public ScoreRuleDialog(){
        super("Score Rule", new String[]{});
        setContent();
    }
    
    @Override
    protected void setContent(String[] key) {
        addButtonAction(ButtonType.CLOSE);
        setStageStyle(StageStyle.DECORATED);
    }
    
    private void setContent(){
        ClassTab tab = new ClassTab("Score");
        tab.setSortBy("ten");
        tab.setContent("setting score rules");
        
        Button addRule = new Button("Add Rule");
        addRule.setOnAction((t) -> {
            try {
                object = (ObjectModel) Class.forName("resourceClass.Score").newInstance();
                System.out.println("new instance: " + object.getProperties());
                ModelFormAction m = new ModelFormAction<>("Add new Score Rule", object.getDeepKeys(), object);
                int type = 0;   //  0 : false, 1: true, 2: false
                while(type==0){
                    switch(m.display().get().toString()){
                        case "true": type = 1;break;
                        case "false": type = 0;break;
                        case "cancel": type = 2;break;
                    }
                }

                if(type==1){
                    object.addToDatabase();
                    tab.setContent("setting score rules");
                }
            } catch (IllegalAccessException | InstantiationException | ClassNotFoundException ex) {
                ex.printStackTrace();
            }
        });
        
        addAll(tab.getContent(), addRule);
    }
}