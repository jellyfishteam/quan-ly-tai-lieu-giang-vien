package ActionDialog;

import GUI.ItemRow;
import java.io.File;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Optional;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.Spinner;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.stage.FileChooser;
import javafx.stage.StageStyle;
import javafx.util.StringConverter;
import managedocumentoflecturer.Method;
import resourceClass.ObjectModel;

/**
 *
 * @author nmhillusion
 * @param <T> must be an object implements OObjectModel
 */
public class ModelFormAction <T extends ObjectModel> extends ModelForm{
    private LinkedHashMap<String, Object> value;
    private boolean validateValue = false;
    private String dataList;
    
    public ModelFormAction(String title, String[] key) {
        super(title, key);
    }
    public ModelFormAction(String title, String[] key, T input) {
        super(title, key, input);
    }
    public ModelFormAction(String title, String[] key, int width, int height) {
        super(title, key, width, height);
    }
    public ModelFormAction(String title, String[] key, T input, int width, int height) {
        super(title, key, input, width, height);
    }
    
    /**
     * Lấy về các thuộc tính của một thực thể nào đó
     * @return một danh sách từng cặp thuộc tính và giá trị của chúng
     */
    protected LinkedHashMap<String, Object> getProperties(){
        return ((ObjectModel)getInput()).getProperties();
    }
    
    @Override
    protected void setContent(String[] primaryKey) {
        value = getProperties();
        
        value.forEach((k, v) -> {
            if(k.compareToIgnoreCase("islive")==0) return;
            if(Arrays.asList(primaryKey).contains(k)){
                TextField txt = new TextField(v.toString());
                txt.setDisable(true);
                addHorizon(new Label(k + ": "), txt);
            }else if(v instanceof Boolean){
                CheckBox cb = new CheckBox(k);
                cb.setSelected(Boolean.valueOf(v.toString()));
                add(cb);
                cb.setOnAction((event) -> {
                    setValueProperties(k, cb.isSelected());
                });
            }else if(v instanceof Integer){
                Spinner<Number> spin = new Spinner<>(1, 1000, (int)v, 1);
                spin.valueProperty().addListener((observable) -> {
                    setValueProperties(k, spin.getValue());
                });
                addHorizon(new Label(k + ": "),spin);
            }else if(v instanceof String && (v.toString()).matches("^:fk-(.*?):\\((.*)\\)$")){
                LinkedHashMap<String, Object> parse = Method.parse2List(v.toString());
                String id = parse.get("value").toString();
                TextField txt = new TextField(id); txt.setDisable(true);
                Button change = new Button("change");
                change.setId("iButton");
                change.setOnAction((event) -> {
                    SelectDialog m = new SelectDialog(parse.get("class").toString(),false);
                    Optional<Object> resDia = m.show();
                    if(resDia.isPresent()){
                        List<ItemRow> newVal = (List<ItemRow>) resDia.get();
                        if(newVal!=null && newVal.size() > 0){
                            String newId = newVal.get(0).getID();
                            if(newId.compareTo(id)!=0){
                                txt.setText(newId);
                                setValueProperties(k, ":fk-" + parse.get("class").toString()+ ":(" + newId + ")");
                            }
                        }
                    }
                });
                
                addHorizon(new Label(k + ": "),txt, change);
            }else if(v instanceof String && (v.toString()).matches("^:fk-(.*?):\\[(.*)\\]$")){
                LinkedHashMap<String, Object> parse = Method.parse2List(v.toString());
                List<String> listItem = new ArrayList<>();
                listItem.addAll(Arrays.asList((String[])parse.get("value")));
                
                ComboBox<String> list = new ComboBox<>();
                list.setPrefSize(70,50);
                list.getItems().addAll(listItem);
                list.getSelectionModel().selectFirst();
                
                Button btnEdit = new Button("Edit");
                btnEdit.setId("iButton");
                btnEdit.setOnAction((event) -> {
                    Optional resDia = new EditListDialog(parse.get("class").toString(), listItem).display();
                    if(resDia.isPresent()){
                        List<String> res = (List<String>) resDia.get();
                        if(res != null && res.size()>0){
                            setValueProperties(k, ":fk-" + parse.get("class") + ":" + res.toString());
                            listItem.clear(); listItem.addAll(res);
                            list.getItems().setAll(listItem);
                        }
                    }
                });
                
                addHorizon(new Label(k + ": "),list, btnEdit);
            }else if(v instanceof LocalDate){
                DatePicker date = new DatePicker(LocalDate.parse(v.toString()));
                date.setConverter(new StringConverter<LocalDate>() {
                    String pattern = "dd/MM/yyyy";
                    DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(pattern);
                    {
                        date.setPromptText(pattern.toLowerCase());
                    }

                    @Override public String toString(LocalDate date) {
                        if (date != null) {
                            return dateFormatter.format(date);
                        } else {
                            return "";
                        }
                    }

                    @Override public LocalDate fromString(String string) {
                        if (string != null && !string.isEmpty()) {
                            return LocalDate.parse(string, dateFormatter);
                        } else {
                            return null;
                        }
                    }
                });
                addHorizon(new Label(k + ": "), date);
                date.valueProperty().addListener((observable) -> {
                    setValueProperties(k, date.getValue());
                });
            }else if(k.trim().compareToIgnoreCase("path")==0){
                TextField txt = new TextField(v.toString());
                txt.setDisable(true);
                Button btnGetFile = new Button("choose file");
                btnGetFile.setId("iButton");
                btnGetFile.setPrefWidth(120);
                
                btnGetFile.setOnAction((event) -> {
                    FileChooser fileChooser = new FileChooser();
                    fileChooser.setTitle("choose file:");
                    File file = fileChooser.showOpenDialog(null);
                    if(file!=null){
                        setValueProperties(k, file.getAbsolutePath());
                        txt.setText(file.getAbsolutePath());
                    }
                });
                
                addHorizon(new Label(k + ": "), txt, btnGetFile);
                txt.textProperty().addListener((observable) -> {
                    setValueProperties(k, txt.getText());
                });
            }else if (k.trim().replaceAll(" ", "").compareToIgnoreCase("schoolYear")==0) {
                TextField y1 = new TextField(v.toString().substring(0, v.toString().indexOf("-"))),
                        y2 = new TextField(v.toString().substring(v.toString().indexOf("-") + 1));
                y1.setStyle("-fx-pref-width: 100;");y2.setStyle("-fx-pref-width: 100;");
                y2.setDisable(true);
                addHorizon(new Label(k + ": "), new HBox(8, y1, new Label("-"),y2));
                y1.textProperty().addListener((observable, o, newVal) -> {
                    if(!newVal.matches("\\d*")){
                        y1.setText(newVal.replaceAll("[^\\d]", ""));
                    }
                    if(y1.getText().length()>0)y2.setText(Integer.valueOf(y1.getText()) + 1 + "");
                    setValueProperties(k, y1.getText() + "-" + y2.getText());
                }); 
            } else {
                TextField txt = new TextField(v.toString().trim());
                addHorizon(new Label(k + ": "), txt);
                txt.textProperty().addListener((observable) -> {
                    setValueProperties(k, txt.getText());
                });
            }
            
//            System.out.println(">> type: " + v + " is " + v.getClass().getTypeName());
        });
        
        addButtonAction(ButtonType.APPLY, ButtonType.CANCEL);
        setStageStyle(StageStyle.DECORATED);
    }
    
    /**
     * cập nhật lại giá trị cho thuộc tính
     * @param k thuộc tính sẽ được cập nhật giá trị
     * @param v giá trị mới sẽ được cập nhật cho thuộc tính
     */
    private void setValueProperties(String k, Object v){
        value.replace(k, v);
    }
    
    /**
     * cài đặt xử lý dữ liệu khi nhấp nút
     * @return kết quả khi đóng form
     */
    @Override
    public Optional<Object> display(){
        setResult((param) -> {
            if(param == ButtonType.APPLY){
                validateValue = validateValue(value);
                if(validateValue){
                    applyValue();
                    return "true";
                }
                else return "false";
            }
            return "cancel";
        });
        return show();
    }
    
    /**
     * Xác nhận dữ liệu vào cơ sở dữ liệu
     */
    protected void applyValue(){
        ((ObjectModel)getInput()).setPropertise(value);
    }
    
    /**
     * Xác nhận tính hợp lệ dữ liệu trước khi đóng form
     * @param value giá trị để kiểm tra
     * @return giá trị muốn thêm có hợp lệ hay không
     */
    protected boolean validateValue(LinkedHashMap<String, Object> value){
        return ((ObjectModel)getInput()).validateValue(value);
    }
    
    /**
     * Có thể cập nhật vào CSDL không, sau khi đã xét tính hợp lệ của dữ liệu
     * @return có thể cập nhật giá trị không
     */
    protected boolean canSet(){
        return validateValue;
    }
    
    /**
     * Lấy về giá trị của danh sách từng cặp <b>thuộc tính - giá trị</b> của đối tượng đang xét
     * @return danh sách từng cặp <b>thuộc tính - giá trị</b> của đối tượng đang xét
     */
    protected LinkedHashMap<String, Object> getValue(){
        return value;
    }
}