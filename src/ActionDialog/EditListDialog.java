package ActionDialog;

import GUI.ClassTab;
import GUI.ItemRow;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.stage.StageStyle;

/**
 *
 * @author nmhillusion
 */
public class EditListDialog extends ModelForm{
    private String searchClass;
    private List<String> listItem = new LinkedList<>();
    private ButtonType btnSave;
    private ClassTab tab;
    
    public EditListDialog(String cls, List<String> input) {
        super("Edit list of" + cls, new String[]{});
        searchClass = cls;
        listItem.addAll(input);
        
        setContent();
    }
    
    @Override
    protected void setContent(String[] key) {
        setStageStyle(StageStyle.DECORATED);
        
        btnSave = new ButtonType("Save", ButtonBar.ButtonData.OK_DONE);
        addButtonAction(btnSave, ButtonType.CANCEL);
    }
    
    private void setContent(){
        tab = new ClassTab(searchClass).setMultySelect(true);
        tab.setContentEditList("editList", listItem, this);
        
        Button btnAdd = new Button("Add New");
        btnAdd.setOnAction((event) -> {
            Optional<Object> resultDialog = new SelectDialog(searchClass, true).display();
            if(resultDialog.isPresent()){
                List<ItemRow> res = (List<ItemRow>)resultDialog.get();
                if(res!=null && res.size() > 0){
                    res.forEach((t) -> {
                        if(!listItem.contains(t.getID()))listItem.add(t.getID());
                    });
                    tab.setContentEditList("editList", listItem, this);
                }
            }
        });
        
        setAll(tab.getContent(), btnAdd);
        
        setResult((btnClicked) -> {
            if(btnClicked == btnSave){
                return listItem;
            }
            return null;
        });
    }

    public void deleteItems(List<String> selectedIDs) {
        if(selectedIDs.size() > 0){
            listItem.removeAll(selectedIDs);
            tab.setContentEditList("editList", listItem, this);
        }
    }
}