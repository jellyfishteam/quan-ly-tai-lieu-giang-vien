package ActionDialog;

import java.util.Arrays;
import java.util.Optional;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Callback;
import managedocumentoflecturer.Method;
import resourceClass.ObjectModel;

/**
 * Đây là mẫu chung cho tất cả các Form trong chương trình chúng ta
 * @author nmhillusion
 * @param <T> type ObjectModel
 */
public abstract class ModelForm <T extends ObjectModel>{
    private final int width = 600, height = 400;
    private Dialog<Object> stage;
    private GridPane grid;
    private short idxContent = 0;
    private T input, result;
    
    /**
     * Phương thức xây dựng cho form
     * @param title tiêu đề form
     * @param key khóa chính của class thực thể mà ta sẽ cho hiển thị trên form này
     */
    protected ModelForm(String title, String[] key){
        initForm(title, key, width, height);
    }
    
    /**
     * Phương thức xây dựng cho form
     * @param title tiêu đề form
     * @param key khóa chính của class thực thể mà ta sẽ cho hiển thị trên form này
     * @param input là tham số đầu vào cho chức năng của form của bạn
     */
    protected ModelForm(String title, String[] key, T input){
        this.input = input;
        initForm(title, key, width, height);
    }
    
    /**
     * Phương thức xây dựng cho form
     * @param title tiêu đề form
     * @param key khóa chính của class thực thể mà ta sẽ cho hiển thị trên form này
     * @param width chiều rộng form tùy chỉnh của bạn
     * @param height chiều cao form tùy chỉnh của bạn
     */
    protected ModelForm(String title, String[] key, int width, int height){
        initForm(title, key,width, height);
    }
    
    /**
    * Phương thức xây dựng cho form
     * @param title tiêu đề form
     * @param key khóa chính của class thực thể mà ta sẽ cho hiển thị trên form này
     * @param input là tham số đầu vào cho chức năng của form của bạn
     * @param width chiều rộng form tùy chỉnh của bạn
     * @param height chiều cao form tùy chỉnh của bạn
     */
    protected ModelForm(String title, String[] key, T input, int width, int height){
        this.input = input;
        initForm(title, key, width, height);
    }
    
    /**
     * Hàm cho chức năng khởi tạo các giá trị ban đầu cho một số biến
     * @param title tiêu đề form
     * @param key khóa chính của class thực thể mà ta sẽ cho hiển thị trên form này
     * @param width chiều rộng form tùy chỉnh của bạn
     * @param height chiều cao form tùy chỉnh của bạn
     */
    private void initForm(String title, String[] key, int width, int height){
        stage = new Dialog<>();
        ((Stage)(stage.getDialogPane().getScene().getWindow())).getIcons().add(Method.ImageFromFile("resource/icon.png"));
        
        grid = new GridPane();
        grid.setPadding(new Insets(10));
        grid.setHgap(8);grid.setVgap(8);
        grid.setAlignment(Pos.CENTER);
        stage.getDialogPane().setContent(new StackPane(grid));
        stage.getDialogPane().getStylesheets().add("manageDocumentOfLecturer/styleSheet.css");
        stage.setTitle(title);
        stage.setOnCloseRequest((event) -> {
            beforeClose();
        });        
        setContent(key);
    }
    
    /**
     * Thêm vào các nút chức năng như Apply, Ok, Cancel...
     * @param buttons danh sách các nút muốn thêm, loại ButtonType
     */
    protected void addButtonAction(ButtonType...buttons){
        stage.getDialogPane().getButtonTypes().addAll(buttons);
    }
    
    protected void setStageStyle(StageStyle style){
        stage.initStyle(style);
    }
    
    /**
     * hiển thị thông tin lên màn hình
     * @return ButtonType which user clicked
     */
    protected Optional<Object> show(){
        return stage.showAndWait();
    }
    
    /**
     * Cài đặt kết quả trả về
     * @param callback function được gọi để lấy giá trị về khi đóng dialog
     */
    protected void setResult(Callback callback){
        stage.setResultConverter(callback);
    }
    
    /**
     * Thiết lập cho hiện thông tin lên (và xử lý việc kiểm soát sự kiện khi nhấp nút nếu cần thiết)
     * @return dang ket qua tra ve
     */
    public Optional<Object> display(){
        return stage.showAndWait();
    }
    
    /**
     * Những thứ sẽ thực hiện trước khi đóng form
     */
    protected void beforeClose(){
        System.out.println("i am closing...." + stage.getTitle());
    }
    
    /**
     * Thực thi việc đóng form lại
     */
    protected void closeForm(){
        beforeClose();
        System.gc();
        stage.close();
    }
    
    /**
     * Lấy về kết quả của hành động mà form này thực hiện
     * @return kết quả sau khi thực hiện form này
     */
    protected T getResult(){
        return result;
    }
    
    /**
     * Lấy về giá trị đầu vào
     * @return giá trị biến đầu vào
     */
    protected T getInput(){
        return input;
    }
    
    /**
     * Hàm này dùng để bạn cài đặt nội dung lên form
     * @param key khóa chính của class thực thể mà ta sẽ cho hiển thị trên form này
     */
    abstract protected void setContent(String[] key);
    
    /**
     * thêm các phần tử trên cùng một hàng
     * @param node - Các phần tử muốn thêm trên cùng hàng
     */
    protected void addHorizon(Node...node){
        if(node.length == 1){
            grid.add(node[0], 1, idxContent++);
        }else{
            int i;
            for(i=0;i<node.length-1;++i){
                grid.add(node[i], i, idxContent);
            }
            grid.add(node[node.length-1], i, idxContent++);
        }
    }
    
    /**
     * Thêm phần tử vào form
     * @param node phần tử sẽ thêm
     */
    protected void add(Node node){
        grid.add(node, 1, idxContent++);
    }
    
    /**
     * Thêm đồng loạt một lượng phần tử bất kỳ vào form, mỗi phần tử sẽ nằm trên một hàng
     * @param node danh sách các phần tử sẽ thêm vào form
     */
    protected void addAll(Node...node){
        Arrays.asList(node).forEach((t) -> {
            grid.add(t, 1, idxContent++);
        });
    }
    
    /**
     * Thay đổi các phần tử trong form, mỗi phần tử sẽ nằm trên một hàng
     * @param node danh sách các phần tử sẽ thêm vào form
     */
    protected void setAll(Node...node){
        grid.getChildren().clear();
        addAll(node);
    }
}