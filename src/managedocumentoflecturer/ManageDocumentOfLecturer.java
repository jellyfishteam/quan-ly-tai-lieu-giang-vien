package managedocumentoflecturer;

import Login.LoginDialog;
import GUI.ClassTab;
import GUI.MainTab;
import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.animation.TranslateTransition;
import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TabPane;
import javafx.scene.effect.DropShadow;
import javafx.scene.effect.Reflection;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.stage.Stage;
import javafx.util.Duration;

/**
 *
 * @author nmhillusion
 */
public class ManageDocumentOfLecturer extends Application{
    private Stage stage;
    private Group content;
    private TabPane tabPane;
    private BorderPane leftBar;
    private MainTab btnManage, btnRecycleBin, btnStatis, btnSetting, btnAbout;
    private Label status;
    private boolean displayingSideBar = false, switchDone = true;
    private String username;
    /**
     * Here is the main of all things on the world
     * @param args input for this program
     */
    public static void main(String[] args) {
        launch(ManageDocumentOfLecturer.class,args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        LoginDialog login = new LoginDialog();
        if(!login.display()) return;
        username = login.getUserName();
        stage = primaryStage;
        stage.setWidth(800);
        stage.setHeight(600);
        stage.setResizable(false);stage.setMaximized(false);
        stage.getIcons().add(Method.ImageFromFile("resource/icon.png"));
        stage.setTitle("aManage");
        
        Label btnOption = new Label("status");
        btnOption.setId("lblStatus");
        
        status = new Label("welcome [username]");
        status.setId("status");
        
        setLeftBar();
        setTabPane();
        
        content = new Group(new VBox(new HBox(btnOption, status),tabPane) , leftBar);
        Scene scene = new Scene(content, Paint.valueOf("#eee"));
        stage.setScene(scene);
        scene.getStylesheets().add("managedocumentoflecturer/styleSheet.css");
        
        content.setOnMouseMoved((event) -> {
            if(!displayingSideBar && event.getX() < 20){
                switchSideBar(true);
            }else if(displayingSideBar && event.getX() > 150){
                switchSideBar(false);
            }
        });
        
        content.setOnMouseExited((event) -> {
            if(displayingSideBar && event.getSceneX() > 150){
                switchSideBar(false);
            }
        });
        
        stage.show();
        
        Timeline timer = new Timeline(new KeyFrame(Duration.seconds(5), (event) -> {
            setStatus();
        }));
        timer.setCycleCount(Animation.INDEFINITE);
        timer.play();
    }
    
    public void setStatus(){
        status.setText(getMainTabSelected() + " > " + tabPane.getSelectionModel().getSelectedItem().getText());
        stage.getScene().setFill(Method.getTheme());
    }
    
    private void setTabPane(){
        ClassTab.setMainProgram(this);
        tabPane = new TabPane(new ClassTab("Student"), new ClassTab("StudentGroup"),
                new ClassTab("Thesis"), new ClassTab("YearProject"), new ClassTab("Essay"),new ClassTab("OtherProject"),new ClassTab("Topic"),new ClassTab("Subject"),
                new ClassTab("Instructor"), new ClassTab("Examiner"), 
                new ClassTab("Exercise"), new ClassTab("Exam"), new ClassTab("LessonPlan"));
        
        tabPane.setPrefWidth(stage.getWidth() - 10);
        tabPane.setPrefHeight(stage.getHeight() - 20);
        tabPane.setId("tab-pane");
        tabPane.setOnMouseMoved((event) -> {
            if(displayingSideBar && event.getX() > 150){
                switchSideBar(false);
            }
        });
    }
    
    private void setLeftBar(){
        leftBar = new BorderPane();
        leftBar.setId("left-bar");
        leftBar.setPrefSize(150, 580);
        leftBar.setTranslateX(-150);
        
        Label welcome = new Label("welcome, " + username + "!");
        welcome.setStyle("-fx-text-fill:#eff;");
        Reflection eff = new Reflection();
        eff.setTopOffset(-3); eff.setFraction(0.4);
        eff.setInput(new DropShadow(3, Color.valueOf("#359bed")));
        welcome.setEffect(eff);
        welcome.setPrefWidth(150);
        welcome.setAlignment(Pos.CENTER);
        welcome.setPadding(new Insets(15, 0, 0, 0));
        
        leftBar.setTop(welcome);
        leftBar.setBottom(new Label("[group's image]"));
        
        btnManage = new MainTab("Manage"); btnManage.setSelected(true);
        btnRecycleBin = new MainTab("Recycle Bin");
        btnStatis = new MainTab("Statistic");
        btnSetting = new MainTab("Setting");
        btnAbout = new MainTab("About");
        
        btnManage.setOnAction((event) -> {
            btnManage.setSelected(true);
            ((ClassTab)tabPane.getSelectionModel().getSelectedItem()).setContent("manage");
            btnRecycleBin.setSelected(false);
            btnStatis.setSelected(false);
        });
        
        btnRecycleBin.setOnAction((event) -> {
            btnRecycleBin.setSelected(true);
            ((ClassTab)tabPane.getSelectionModel().getSelectedItem()).setContent("recycle bin");
            btnManage.setSelected(false);
            btnStatis.setSelected(false);
        });
        
        btnStatis.setOnAction((event) -> {
            btnStatis.setSelected(true);
            ((ClassTab)tabPane.getSelectionModel().getSelectedItem()).setContent("statistic");
            btnRecycleBin.setSelected(false);
            btnManage.setSelected(false);
        });
        
        //  setting for maintab
        btnSetting.setOnMousePressed((event) -> {
            Method.setting(btnSetting, event.getScreenX(), event.getScreenY());
        });
        
        VBox mainTabs = new VBox(4, btnManage, btnRecycleBin, btnStatis, btnSetting, btnAbout);
        mainTabs.setPrefSize(leftBar.getPrefWidth(), leftBar.getPrefHeight());
        mainTabs.setAlignment(Pos.CENTER_LEFT);
        
        leftBar.setCenter(mainTabs);
        ImageView icon = new ImageView(Method.ImageFromFile("resource/iconBG.png"));
        icon.setFitWidth(80);icon.setFitHeight(80);
        leftBar.setBottom(icon);
    }
    
    public String getMainTabSelected(){
        String res ="";
        if(btnManage.isSelected()){
            res = "manage";
        }else if(btnRecycleBin.isSelected()){
            res = "recycle bin";
        }else if(btnStatis.isSelected()){
            res = "statistic";
        }
        
        return res;
    }
    
    private void switchSideBar(boolean fadeIn){
        if(switchDone) if(fadeIn){
            switchDone = false;
            TranslateTransition st = new TranslateTransition(Duration.seconds(0.3), leftBar);
            st.setFromX(-150);
            st.setToX(0);
            st.play();
            displayingSideBar = true;
            
            st.setOnFinished((event) -> {
                switchDone = true;
            });
        }else{
            switchDone = false;
            TranslateTransition st = new TranslateTransition(Duration.seconds(0.3), leftBar);
            st.setFromX(0);
            st.setToX(-150);
            st.play();
            displayingSideBar = false;
            
            st.setOnFinished((event) -> {
                switchDone = true;
            });
        }
    }
}