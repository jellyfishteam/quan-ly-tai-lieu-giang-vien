package managedocumentoflecturer;

import ActionDialog.ScoreRuleDialog;
import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Dialog;
import javafx.scene.control.MenuItem;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;

/**
 *
 * @author nmhillusion
 */
public class Method {
    /**
     * Lấy về một hình ảnh từ đường dẫn cung cấp
     * @param path đường dẫn đên hình ảnh
     * @return hình ảnh lấy về được
     */
    public static Image ImageFromFile(String path) {
        Image img = null;
        try {
            img = new Image(new File(path).toURI().toURL().toString());
        } catch (java.net.MalformedURLException ex) {
            System.out.println("Path to image is not available!");
        }
        return img;
    }
    
    public static Connection getConnection(){
        Connection con = null;
        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            String dbUrl = "jdbc:sqlserver://nmhillusion-pc\\sqlexpress:1433;databaseName=ManageDocumentOfLecturer;user=sa;password=sa2008";
            con = DriverManager.getConnection(dbUrl);
            
            System.out.println(" connected database...");
        }catch(ClassNotFoundException|SQLException ex){
            System.out.println("// error in get connection! ");
            ex.printStackTrace();
        }
        return con;
    }
    
    /**
     * Hàm xóa đi tất cả các khoảng trong trên 2 ký tự trong chuỗi
     * @param val chuỗi đầu vào
     * @return chuỗi sau khi được chuyển
     */
    public static String delete2space(String val){
        val = val.trim();
        if(val.contains("  ")){
            return delete2space(val.replaceAll("  ", " "));
        }else{
            return val;
        }
    }
    
    public static LinkedHashMap<String, Object> parse2List(String val){
        LinkedHashMap<String, Object> res = new LinkedHashMap<>();
        Pattern p = Pattern.compile("^:fk-(.*?):");
        Matcher m = p.matcher(val);
            
        if(val.matches("^:fk-(.*?):\\((.*)\\)$")){
            res.put("type", "single");
            
            if(m.find()){
                val = m.replaceFirst("");
                res.put("class", m.group().replaceAll(":", "").replaceAll("fk-", ""));
                String id = val.substring(1, val.length()-1);
                res.put("value", id);
                if(id.length() > 0) try {
                    Connection con = getConnection();
                    Statement s = con.createStatement();
                    ResultSet rs = s.executeQuery("select * from " + res.get("class") + " where ID='" + id + "'");
                    
                    if(rs.next()){
                        res.put("value-display", id + " - " + rs.getString(2).trim());
                    }
                    con.close();
                } catch (SQLException e) {
                    System.out.println("//  error in get display foreign key method");
                }
            }
        }else if(val.matches("^:fk-(.*?):\\[(.*)\\]$")){
            res.put("type", "list");
            
            if(m.find()){
                val = m.replaceFirst("");
                res.put("class", m.group().replaceAll(":", "").replaceAll("fk-", ""));
                
                String[] value = new String[]{};
                if(val.substring(1, val.length()-1).compareTo("")!=0){
                    value = val.substring(1, val.length()-1).split(", ");
                }
                res.put("value", value);
                
                if(value.length > 0) try {
                    Connection con = getConnection();
                    Statement s = con.createStatement();
                    List<String> value_display = new ArrayList<>();
                    
                    for(String id : value){
                        ResultSet rs = s.executeQuery("select * from " + res.get("class") + " where ID='" + id + "'");

                        if(rs.next()){
                            value_display.add(id + " - " + rs.getString(2).trim());
                        }
                    }
                    res.put("value-display", value_display);
                    con.close();
                } catch (SQLException e) {
                    System.out.println("//  error in get display foreign key method");
                }
            }
        }
        return res;
    }
    
    public static void setting(Button menu, double x, double y){
        MenuItem background = new MenuItem("Change background"),
                settingScore = new MenuItem("Change Score Rules");
        
        background.setOnAction((event) -> {
            changeTheme();
        });
        
        settingScore.setOnAction((event) -> {
            new ScoreRuleDialog().display();
        });
        
        new ContextMenu(background, settingScore).show(menu, x, y);
    }
    
    private static String theme = "#eee";
    public static void changeTheme(){
        ColorPicker c = new ColorPicker(Color.valueOf(theme));
        c.valueProperty().addListener((observable) -> {
            theme = c.getValue().toString();
            try(Connection con = getConnection()){
                Statement s = con.createStatement();
                s.executeUpdate("update account set theme='" + theme + "'");
            }catch(SQLException ex){}
            
        });
        Dialog d = new Dialog();
        d.setHeaderText("Change background color: ");
        d.getDialogPane().setContent(c);
        d.getDialogPane().getButtonTypes().add(ButtonType.CLOSE);
        d.show();
    }
    
    public static Color getTheme(){
        return Color.valueOf(theme);
    }
    
    public enum DialogType{
        ALERT, INFO, ERROR, CONFIRM
    }
    
    public static boolean Dialog(DialogType type, String title, String content){
        boolean res = true;
        Alert dialog = new Alert(Alert.AlertType.NONE);
        
        switch(type){
            case ALERT:
                dialog = new Alert(Alert.AlertType.WARNING, content, ButtonType.CLOSE);
                break;
                
            case INFO:
                dialog = new Alert(Alert.AlertType.INFORMATION, content, ButtonType.CLOSE);
                break;
                
            case ERROR:
                dialog = new Alert(Alert.AlertType.ERROR, content, ButtonType.CLOSE);
                break;
                
            case CONFIRM:
                dialog = new Alert(Alert.AlertType.CONFIRMATION, content, ButtonType.YES, ButtonType.NO);
                break;
        }
        
        dialog.setTitle(title);
        Optional<ButtonType> resDia = dialog.showAndWait();
        return resDia.isPresent() && resDia.get() == ButtonType.YES;
    }
    
    @SuppressWarnings("empty-statement")
    public static List<Object> switchScore(double val){
        List<Object> res = new  ArrayList<>();
        
        try {
            try(Connection con = getConnection()){
                Statement s = con.createStatement();
                ResultSet rs = s.executeQuery("select * from Score order by ten desc");
                
                while (rs.next()){
                    if(rs.getDouble("ten") <= val){
                        res.add(rs.getString("word"));res.add(rs.getDouble("four"));
                        break;
                    }
                }
                
                con.close();
            }
        } catch (SQLException e) {
        }
        
        return res;
    }
    
    public static String moveFile(String src, String targetFolder){
        File srcf = new File(src),
                newFile = new File(targetFolder + "\\"+ srcf.getName());
        
        if(srcf.renameTo(newFile)){
            System.out.println("Moved");
            return newFile.getAbsolutePath();
        }else{
            System.out.println("Can not move");
            return "";
        }
    }
}