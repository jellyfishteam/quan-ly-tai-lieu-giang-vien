package managedocumentoflecturer;

import java.util.LinkedHashMap;

/**
 *
 * @author Kieu Nhut Truong
 */
public class Statistic {

    public static LinkedHashMap exec(String Class) {
        LinkedHashMap<String, String> result = new LinkedHashMap();

        try {
            switch (Class) {
                // Topic
                case ("Topic"): {
                    String query = "select ID,name,sum(data) as data from (\n"
                            + "\n"
                            + "select Topic.ID,Topic.name,count(Topic.ID) as data from Topic inner join OtherProject on Topic.ID=OtherProject.topicID where OtherProject.isLive=1 group by Topic.ID,Topic.name\n"
                            + "union\n"
                            + "select Topic.ID,Topic.name,count(Topic.ID) as data from Topic inner join Thesis on Topic.ID=Thesis.topicID where Thesis.isLive=1 group by Topic.ID,Topic.name\n"
                            + "union\n"
                            + "select Topic.ID,Topic.name,count(Topic.ID) as data from Topic inner join Essay on Topic.ID=Essay.topicID where Essay.isLive=1 group by Topic.ID,Topic.name\n"
                            + "union\n"
                            + "select Topic.ID,Topic.name,count(Topic.ID) as data from Topic inner join YearProject on Topic.ID=YearProject.topicID where YearProject.isLive=1 group by Topic.ID,Topic.name\n"
                            + ") temp\n"
                            + "\n"
                            + "group by ID,name order by ID";
                    result.put("Topic", query);
                    break;
                }
                //Year Project
                case ("YearProject"): {
                    String query1 = "select schoolYear,count(schoolYear) as data from " + Class + " where " + Class + ".isLive=1 group by schoolYear";
                    result.put("School Year", query1);
                    String query2 = "select wordScore,count(wordScore) as data from " + Class + " where " + Class + ".isLive=1 group by wordScore";
                    result.put("Word Score", query2);
                    String query3 = "select Topic.ID,Topic.name,count(Topic.ID) as data from Topic inner join " + Class + " on Topic.ID=" + Class + ".topicID where " + Class + ".isLive=1 group by Topic.ID,Topic.name";
                    result.put("Topic", query3);
                    String query4 = "select Subject.ID,Subject.subjectName,count(Subject.ID) as data from Subject inner join " + Class + " on Subject.ID=" + Class + ".subjectID where " + Class + ".isLive=1 group by Subject.ID,Subject.subjectName";
                    result.put("Subject", query4);
                    break;
                }
                //Other Project
                case ("OtherProject"): {
                    String query1 = "select schoolYear,count(schoolYear) as data from " + Class + " where " + Class + ".isLive=1 group by schoolYear";
                    result.put("School Year", query1);
                    String query2 = "select wordScore,count(wordScore) as data from " + Class + " where " + Class + ".isLive=1 group by wordScore";
                    result.put("Word Score", query2);
                    String query3 = "select Topic.ID,Topic.name,count(Topic.ID) as data from Topic inner join " + Class + " on Topic.ID=" + Class + ".topicID where " + Class + ".isLive=1 group by Topic.ID,Topic.name";
                    result.put("Topic", query3);
                    String query4 = "select Subject.ID,Subject.subjectName,count(Subject.ID) as data from Subject inner join " + Class + " on Subject.ID=" + Class + ".subjectID where " + Class + ".isLive=1 group by Subject.ID,Subject.subjectName";
                    result.put("Subject", query4);
                    break;
                }
                //Essay
                case ("Essay"): {
                    String query1 = "select schoolYear,count(schoolYear) as data from " + Class + " where " + Class + ".isLive=1 group by schoolYear";
                    result.put("School Year", query1);
                    String query2 = "select wordScore,count(wordScore) as data from " + Class + " where " + Class + ".isLive=1 group by wordScore";
                    result.put("Word Score", query2);
                    String query3 = "select Topic.ID,Topic.name,count(Topic.ID) as data from Topic inner join " + Class + " on Topic.ID=" + Class + ".topicID where " + Class + ".isLive=1 group by Topic.ID,Topic.name";
                    result.put("Topic", query3);
                    String query4 = "select Subject.ID,Subject.subjectName,count(Subject.ID) as data from Subject inner join " + Class + " on Subject.ID=" + Class + ".subjectID where " + Class + ".isLive=1 group by Subject.ID,Subject.subjectName";
                    result.put("Subject", query4);
                    break;
                }
                //Thesis
                case ("Thesis"): {
                    String query1 = "select schoolYear,count(schoolYear) as data from " + Class + " where " + Class + ".isLive=1 group by schoolYear";
                    result.put("School Year", query1);
                    String query2 = "select wordScore,count(wordScore) as data from " + Class + " where " + Class + ".isLive=1 group by wordScore";
                    result.put("Word Score", query2);
                    String query3 = "select Topic.ID,Topic.name,count(Topic.ID) as data from Topic inner join " + Class + " on Topic.ID=" + Class + ".topicID where " + Class + ".isLive=1 group by Topic.ID,Topic.name";
                    result.put("Topic", query3);
                    String query4 = "select Subject.ID,Subject.subjectName,count(Subject.ID) as data from Subject inner join " + Class + " on Subject.ID=" + Class + ".subjectID where " + Class + ".isLive=1 group by Subject.ID,Subject.subjectName";
                    result.put("Subject", query4);
                    break;
                }
                //Student
                case ("Student"): {
                    String query1 = "select major,count(major) as data from " + Class + " where " + Class + ".isLive=1 group by major";
                    result.put("Major Name", query1);
                    String query2 = "select isMale,count(isMale) as data from " + Class + " where " + Class + ".isLive=1 group by isMale";
                    result.put("Is Male", query2);
                    String query3 = "select college,count(college) as data from " + Class + " where " + Class + ".isLive=1 group by college";
                    result.put("College", query3);
                    String query4 = "select Class,count(Class) as data from " + Class + " where " + Class + ".isLive=1 group by Class";
                    result.put("Class", query4);
                    String query5 = "select course,count(course) as data from " + Class + " where " + Class + ".isLive=1 group by course";
                    result.put("Course", query5);
                    break;
                }
                //Examiner
                case ("Examiner"): {
                    String query = "select occupation,count(occupation) as data from Examiner group by occupation";
                    result.put("Examiner", query);
                    break;
                }
                //Instructor
                case ("Instructor"): {
                    String query = "select occupation,count(occupation) as data from Instructor group by occupation";
                    result.put("Instructor", query);
                    break;
                }
                //LessonPlan
                case ("LessonPlan"): {
                    String query1 = "select Subject.ID,Subject.subjectName,count(Subject.ID) as data from Subject inner join LessonPlan on Subject.ID=LessonPlan.subjectID group by Subject.ID,Subject.subjectName";
                    result.put("Subject", query1);
                    String query2 = "select author,count(author) as data from LessonPlan group by author";
                    result.put("Author", query2);
                    break;
                }
                //Exercise
                case ("Exercise"): {
                    String query = "select Subject.ID,Subject.subjectName,count(Subject.ID) as data from Subject inner join Exercise on Subject.ID=Exercise.subjectID group by Subject.ID,Subject.subjectName";
                    result.put("Subject", query);
                    break;
                }
                //Exam
                case ("Exam"): {
                    String query1 = "select Subject.ID, Subject.subjectName, count(Subject.ID) as data from Subject inner join Exam on Subject.ID=Exam.subjectID group by Subject.ID,Subject.subjectName";
                    result.put("Subject", query1);
                    String query2 = "select schoolYear,count(schoolYear) as data from Exam group by schoolYear";
                    result.put("School Year", query2);
                    break;
                }
                
            }
        } catch (Exception e) {
            result = null;
        }
        return result;
    }
}