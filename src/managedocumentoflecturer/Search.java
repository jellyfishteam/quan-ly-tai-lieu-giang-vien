package managedocumentoflecturer;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author dell
 */
public class Search {
    //T data;
    Connection con=Method.getConnection();
    String query;
    //String resourceclass,field,searchtext;
    
    /*public search(String resourceclass, String field, String searchtext){
        this.field=field;
        this.searchtext=searchtext;
        this.resourceclass=resourceclass;
    }
   public search(){
       
   }*/
   
   public ResultSet normalsearch(String resourceclass, String field, Object searchtext){
        ResultSet rs=null;
        try {
           Statement s = con.createStatement();
           //System.out.println("select * from "+resourceclass+" where "+field+" like '%"+searchtext+"%' order by "+field);
           String fieldS = field.trim().toLowerCase().replaceAll(" ", "");
           if(searchtext instanceof Boolean){
               System.out.println("boolean>>");
               s.execute("select * from "+resourceclass+" where "+fieldS+" = '"+searchtext+"' order by "+fieldS);
           }
           else {
               System.out.println("<< not boolean : " + searchtext.getClass());
               s.execute("select * from "+resourceclass+" where "+fieldS+" like '%"+searchtext+"%' order by "+fieldS);
           }
           rs=s.getResultSet();
           
           return rs;
            
        } catch (SQLException ex) {
            Logger.getLogger(Search.class.getName()).log(Level.SEVERE, null, ex);
        }
       
       return rs;
       
   }
   
   public ResultSet advancedsearch(String resourceclass, LinkedHashMap<String , Object> list){
       ResultSet rs=null;
       
        try {
            query = "select * from " + resourceclass + " where ";
            list.forEach((t, u) -> {
                query += t.trim().toLowerCase().replaceAll(" ", "") + " like '%" + u + "%' and ";
            });
            query=query.substring(0, query.length()-5);//+";";
            System.out.println("DDDDDDDDDDDDDDDDDDDDDDDDDDDDD  "+query+"   DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD");
            Statement s=con.createStatement();
            s.execute(query);
            rs=s.getResultSet();
            
            return rs;
        } catch (SQLException ex) {
            Logger.getLogger(Search.class.getName()).log(Level.SEVERE, null, ex);
        }
       
       return rs;
   }
   
}